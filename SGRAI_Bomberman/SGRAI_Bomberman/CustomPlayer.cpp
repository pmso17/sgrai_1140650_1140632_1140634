#include "stdafx.h"
#include "time.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include "SOIL.h"
#include "SGRAI_Bomberman.h"
#include "CustomController.h"
#include "Sound.h"
#include "Configurations.h"

using namespace std;

/* VARIAVEIS GLOBAIS */

#define DEBUG 1
#define FPS 24
#define TAMANHO_MAPA 50
#define NOME_TEXTURA_MENU_BG "Data/bg.png"
#define NOME_TEXTURA_BOTAO "Data/pavement0.png"
#define NOME_TEXTURA_BOTAO_OK "Data/ok.png"


namespace CustomPlayer {

	typedef struct {
		GLfloat    x, y, z;
	}Pos;

	typedef struct teclas_t {
		GLboolean   w, a, s, d,
			i, j, k, l,
			up, down, left, right;
	}Teclas;

	typedef struct {
		GLboolean	timerFlag;
		Sound*		music;
		GLboolean   doubleBuffer;
		GLint       delayMovimento;
		GLboolean   debug;
		teclas_t    teclas;
		GLfloat		ratioWidth, ratioHeight;
	}Estado;

	Estado estado;

	GLfloat texCoords[][2] = {
		{ 0.0f, 0.0f },  // Canto Inferior Esquerdo
		{ 1.0f, 0.0f },  // Canto Inferior Direito
		{ 1.0f, 1.0f },   // Canto Superior Direito
		{ 0.0f, 1.0f }  // Canto Superior Esquerdo
	};

	GLint texture[4];

	CustomController* controller;

	Pos angle;

	int LoadGLTextures()                                    // Load Bitmaps And Convert To Textures
	{
		/* load an image file directly as a new OpenGL texture */
		texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[1] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[2] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[3] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO_OK, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

		if (texture[0] == 0) {
			return false;
		}


		// Typical Texture Generation Using Data From The Bitmap
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[3]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return true;
	}

	////////////////////////////////////
	/// Ilumina��o e materiais


	void setLight()
	{
		GLfloat light_pos[4] = { -5.0f, 20.0f, -8.0f, 0.0f };
		GLfloat light_ambient[] = { 0.4f, 0.4f, 0.4f, 1.0f };
		GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
		GLfloat light_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
		// laar ilumina��o
		glEnable(GL_LIGHTING);
		// ligar e definir fonte de light 0
		glEnable(GL_LIGHT0);
		glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
		glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	}

	void setMaterial()
	{
		GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
		GLfloat mat_shininess = 104;
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
		// definir os outros par�metros estaticamente
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
	}

	/* CALLBACK PARA REDIMENSIONAR JANELA */
	void Reshape(int width, int height)
	{
		estado.ratioHeight = height / 700.0f;
		estado.ratioWidth = width / 700.0f;
		glViewport(0, 0, (GLint)width, (GLint)height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-5, 5, -5 * (GLdouble)height / width, 5 * (GLdouble)height / width, -10, 10);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	void desenhaBackGround()
	{
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glBegin(GL_POLYGON);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(-5, -5, 0);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(5, -5, 0);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(5, 5, 0);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(-5, 5, 0);
		glEnd();
	}

	void desenhaButton(GLfloat minX, GLfloat maxX, GLfloat minY, GLfloat maxY, GLint texNumber) {
		glBindTexture(GL_TEXTURE_2D, texture[texNumber]);
		glBegin(GL_POLYGON);
		//glColor3f(1,1,1);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(minX, minY, 0.1);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(maxX, minY, 0.1);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(maxX, maxY, 0.1);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(minX, maxY, 0.1);
		glEnd();
	}

	void desenhaSeta() {
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, texture[1]);
		glBegin(GL_POLYGON);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(-0.2, -1, 1);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(-0.2, 1, 1);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(1.8, 1, 1);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(1.8, -1, 1);
		glEnd();

		glColor3f(0.3, 0.3, 0.3);
		glBegin(GL_TRIANGLE_FAN);
		glVertex3f(0, 0, 1.1);
		glVertex3f(1, 0, 1.1);
		glVertex3f(1.5, 1, 1.1);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glVertex3f(0, 0, 1.1);
		glVertex3f(1, 0, 1.1);
		glVertex3f(1.5, -1, 1.1);
		glEnd();
		glColor3f(1, 1, 1);

		glPopMatrix();
	}

	/* Callback de desenho */
	void Draw(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		/* ... chamada das rotinas auxiliares de desenho ... */
		glPushMatrix();
		desenhaBackGround();

		glPushMatrix();
		glTranslatef(-3, 0, 0);
		glScalef(0.5, 0.5, 1);
		desenhaSeta();
		glPopMatrix();

		desenhaButton(-2, 2, -4.5, -3.75, 3);

		glPushMatrix();
		glTranslatef(3, 0, 0);
		glScalef(-0.5, 0.5, 1);
		desenhaSeta();
		glPopMatrix();

		glTranslatef(0, 0, 3);
		glRotatef(angle.x, 1, 0, 0);
		glRotatef(angle.y, 0, 1, 0);
		glRotatef(angle.z, 0, 0, 1);
		glScalef(1, 1, 1);
		controller->Draw();
		glPopMatrix();

		if (estado.doubleBuffer)
			glutSwapBuffers();
		else
			glFlush();

	}

	/*******************************
	***   callbacks timer   ***
	*******************************/
	/* Callback de temporizador */
	void Timer(int value)
	{
		clock_t drawStartTime = clock();
		glutPostRedisplay();
		clock_t drawEndTime = clock();

		int delayToNextFrame = (CLOCKS_PER_SEC / FPS) - (drawEndTime - drawStartTime);
		delayToNextFrame = floor(delayToNextFrame + 0.5);
		delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;

		if (estado.timerFlag)
		{
			glutTimerFunc(delayToNextFrame, Timer, 0);
		}
		/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
		somente os valores das vari�veis ... */

		if (estado.teclas.i)
		{
			angle.x += 5;
		}

		if (estado.teclas.k)
		{
			angle.x -= 5;
		}

		if (estado.teclas.j)
		{
			angle.y += 5;
		}

		if (estado.teclas.l)
		{
			angle.y -= 5;
		}

		if (estado.teclas.w)
		{
			controller->Andar();
		}

		if (estado.teclas.left)
		{
			controller->previousConfiguration();
		}

		if (estado.teclas.right)
		{
			controller->nextConfiguration();
		}

		/* redesenhar o ecr� */
		glutPostRedisplay();
	}

	/* Callback para interaccao via teclado (carregar na tecla) */
	void Key(unsigned char key, int x, int y)
	{
		switch (key) {
		case 27:
		case 13:
			Configurations::setTextureNumber1(controller->getActualConfiguration());
			delete controller;
			estado.music->StopAudio();
			estado.timerFlag = GL_FALSE;
			MainInit();
			/* ... accoes sobre outras teclas ... */
			break;
		case 'W':
		case 'w':
			estado.teclas.w = GL_TRUE;
			break;
		case 'A':
		case 'a':
			estado.teclas.a = GL_TRUE;
			break;
		case 'S':
		case 's':
			estado.teclas.s = GL_TRUE;
			break;
		case 'D':
		case 'd':
			estado.teclas.d = GL_TRUE;
			break;

		case 'I':
		case 'i':
			estado.teclas.i = GL_TRUE;
			break;
		case 'J':
		case 'j':
			estado.teclas.j = GL_TRUE;
			break;
		case 'K':
		case 'k':
			estado.teclas.k = GL_TRUE;
			break;
		case 'L':
		case 'l':
			estado.teclas.l = GL_TRUE;
			break;
		}

		if (DEBUG) {
			printf("Carregou na tecla %c\n", key);
		}
	}

	/* Callback para interaccao via teclado (largar a tecla) */
	void KeyUp(unsigned char key, int x, int y)
	{
		switch (key)
		{
		case 'W':
		case 'w':
			estado.teclas.w = GL_FALSE;
			break;
		case 'A':
		case 'a':
			estado.teclas.a = GL_FALSE;
			break;
		case 'S':
		case 's':
			estado.teclas.s = GL_FALSE;
			break;
		case 'D':
		case 'd':
			estado.teclas.d = GL_FALSE;
			break;

		case 'I':
		case 'i':
			estado.teclas.i = GL_FALSE;
			break;
		case 'J':
		case 'j':
			estado.teclas.j = GL_FALSE;
			break;
		case 'K':
		case 'k':
			estado.teclas.k = GL_FALSE;
			break;
		case 'L':
		case 'l':
			estado.teclas.l = GL_FALSE;
			break;
		}

		if (DEBUG)
			printf("Largou a tecla %c\n", key);
	}

	/* Callback para interaccao via teclas especiais  (carregar na tecla) */
	void SpecialKey(int key, int x, int y)
	{
		/* ... accoes sobre outras teclas especiais ...
		GLUT_KEY_F1 ... GLUT_KEY_F12
		GLUT_KEY_UP
		GLUT_KEY_DOWN
		GLUT_KEY_LEFT
		GLUT_KEY_RIGHT
		GLUT_KEY_PAGE_UP
		GLUT_KEY_PAGE_DOWN
		GLUT_KEY_HOME
		GLUT_KEY_END
		GLUT_KEY_INSERT
		*/

		switch (key) {
			/*case GLUT_KEY_UP:estado.teclas.up = GL_TRUE;
				break;
			case GLUT_KEY_DOWN:estado.teclas.down = GL_TRUE;
				break;*/
		case GLUT_KEY_LEFT:estado.teclas.left = GL_TRUE;
			break;
		case GLUT_KEY_RIGHT:estado.teclas.right = GL_TRUE;
			break;
		}


		if (DEBUG)
			printf("Carregou na tecla especial %d\n", key);
	}

	/* Callback para interaccao via teclas especiais (largar na tecla) */
	void SpecialKeyUp(int key, int x, int y)
	{

		switch (key) {
			/*case GLUT_KEY_UP:estado.teclas.up = GL_FALSE;
				break;
			case GLUT_KEY_DOWN:estado.teclas.down = GL_FALSE;
				break;*/
		case GLUT_KEY_LEFT:estado.teclas.left = GL_FALSE;
			break;
		case GLUT_KEY_RIGHT:estado.teclas.right = GL_FALSE;
			break;
		}

		if (DEBUG)
			printf("Largou a tecla especial %d\n", key);

	}

	void Mouse(int button, int state, int x, int y)
	{
		/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
		state  => GLUT_UP, GLUT_DOWN
		x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
		*/

		if (state == GLUT_UP)
		{
			switch (button) {
			case GLUT_LEFT_BUTTON:
				if (x >= 135 *estado.ratioWidth && x <= 200 * estado.ratioWidth && y >= 315 * estado.ratioHeight && y <= 385 * estado.ratioHeight) //Previous
				{
					controller->previousConfiguration();
				}

				if (x >= 495 * estado.ratioWidth && x <= 565 * estado.ratioWidth && y >= 315 * estado.ratioHeight && y <= 385 * estado.ratioHeight) //Next
				{
					controller->nextConfiguration();
				}

				if (x >= 210 * estado.ratioWidth && x <= 490 * estado.ratioWidth && y >= 610 * estado.ratioHeight && y <= 655 * estado.ratioHeight)//Ok
				{
					Configurations::setTextureNumber1(controller->getActualConfiguration());
					delete controller;
					estado.music->StopAudio();
					estado.timerFlag = GL_FALSE;
					MainInit();
				}
				break;
			case GLUT_MIDDLE_BUTTON:

				break;
			case GLUT_RIGHT_BUTTON:

				break;
			}
		}
		if (DEBUG)
			printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
	}

	void Init() {
		estado.music = new Sound("Data/Music/custom.wav",0);
		estado.music->InitAudio(true);
		estado.ratioHeight = 1;
		estado.ratioWidth = 1;
		//setLight();
		//setMaterial();
		controller = new CustomController();
		estado.timerFlag = GL_TRUE;
		estado.doubleBuffer = 1;
		estado.delayMovimento = 50;
		//glDisable(GL_TEXTURE_2D);
		/* Registar callbacks do GLUT */
		LoadGLTextures();
		/* callbacks de janelas/desenho */
		glutReshapeFunc(Reshape);
		glutDisplayFunc(Draw);

		/* Callbacks de teclado */
		glutKeyboardFunc(Key);
		glutKeyboardUpFunc(KeyUp);
		glutSpecialFunc(SpecialKey);
		glutSpecialUpFunc(SpecialKeyUp);

		/* callbacks rato */
		//glutPassiveMotionFunc(MousePassiveMotion);
		//glutMotionFunc(MouseMotion);
		glutMouseFunc(Mouse);

		/* callbacks timer/idle */
		glutTimerFunc(estado.delayMovimento, Timer, 0);
		//glutIdleFunc(Idle);
	}
}

