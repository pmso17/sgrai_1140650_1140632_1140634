#pragma once
#include "operation.h"
class add : public operation
{
public:
	add();
	~add();
	void operate(int* add, int* toThis) override;
	void reverse(int* add, int* toThis) override;
};