#include "stdafx.h"
#include "PartBuilder.h"

#include "SOIL.h"

PartBuilder::PartBuilder()
{
}


PartBuilder::~PartBuilder()
{
}

void desenhaPoligono(GLint texture,GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[])
{
	GLfloat texCoords[][2] = {
		{ 0.0f, 0.0f },  // Canto Inferior Esquerdo
		{ 1.0f, 0.0f },  // Canto Inferior Direito
		{ 1.0f, 1.0f },   // Canto Superior Direito
		{ 0.0f, 1.0f }  // Canto Superior Esquerdo
	};

	/* draw a polygon via list of vertices */
	glBindTexture(GL_TEXTURE_2D,texture);
	glBegin(GL_POLYGON);
	
	glTexCoord2fv(texCoords[0]);
	glVertex3fv(a);
	glTexCoord2fv(texCoords[1]);
	glVertex3fv(b);
	glTexCoord2fv(texCoords[2]);
	glVertex3fv(c);
	glTexCoord2fv(texCoords[3]);
	glVertex3fv(d);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void PartBuilder::drawCube(GLint* textures) {
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };

	glNormal3f(0, 0, 1);
	desenhaPoligono(textures[1],vertices[1], vertices[0], vertices[3], vertices[2]);//Atr�s
	glNormal3f(0, 0, -1);
	desenhaPoligono(textures[0], vertices[4], vertices[5], vertices[6], vertices[7]);//Frente
	
	glPushMatrix();//Acerto de textura
	glRotatef(90,1,0,0);
	glNormal3f(1, 0, 0);
	desenhaPoligono(textures[2], vertices[3], vertices[0], vertices[4], vertices[7]);//Lateral Esquerda
	glPopMatrix();
	
	glPushMatrix();
	glScalef(1,1,-1);
	glNormal3f(-1, 0, 0);
	desenhaPoligono(textures[2], vertices[2], vertices[6], vertices[5], vertices[1]);//Lateral Direita
	glPopMatrix();
	glNormal3f(0, 1, 0);
	desenhaPoligono(textures[3], vertices[2], vertices[3], vertices[7], vertices[6]);//Cima
	glNormal3f(0, -1, 0);
	desenhaPoligono(textures[4], vertices[5], vertices[4], vertices[0], vertices[1]);//Baixo
}