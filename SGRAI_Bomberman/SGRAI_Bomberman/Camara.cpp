#include "stdafx.h"
#include "Camara.h"


Camara::Camara()
{
}


Camara::~Camara()
{
}

void Camara::changeCamera()
{
	modoCamera = !modoCamera;
}

void Camara::setCamera(GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara, float* posPlayer) {
	x = (sin(dtr(rotacaoCamaraHorizontal))*raioCamara);
	y = (sin(dtr(rotacaoCamaraVertical))*raioCamara);
	z = (cos(dtr(rotacaoCamaraHorizontal))*raioCamara);
	if (modoCamera)
	{
		glScalef(Configurations::getScaleCamera(), Configurations::getScaleCamera(), Configurations::getScaleCamera());
		gluLookAt(x + posPlayer[0], 0.5 + y + 0.8, -z + posPlayer[2],
			posPlayer[0], 0.5, posPlayer[2],
			0, 1, 0);
	}
	else {
		gluLookAt(x + posPlayer[0], 0.5 + y, -z + posPlayer[2],
			posPlayer[0], 0.5, posPlayer[2],
			0, 1, 0);
	}
}