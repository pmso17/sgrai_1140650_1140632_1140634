#pragma once
#include "Bomb.h"
class CrossBomb :public Bomb
{
public:
	CrossBomb(GLint xPos, GLint zPos);
	~CrossBomb();
	std::vector<GLint*> Explode() override;
};

