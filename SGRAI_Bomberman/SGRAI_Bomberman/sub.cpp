#include "stdafx.h"
#include "sub.h"

sub::sub():operation()
{
}

void sub::operate(int* add, int* toThis) {
	(*add)++;
	(*toThis)--;
}

void sub::reverse(int* add, int* toThis) {
	(*add)--;
	(*toThis)++;
}
