#include "stdafx.h"
#include "RandomBomb.h"


RandomBomb::RandomBomb(GLint xPos, GLint zPos):Bomb(xPos,zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/bombRandom.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	bombType = 1;
}


RandomBomb::~RandomBomb()
{
}
