#include "stdafx.h"
#include "AtomicBomb.h"


AtomicBomb::AtomicBomb(GLint xPos, GLint zPos) :Bomb(xPos, zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/atomicBomb.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	bombType = 5;
}


AtomicBomb::~AtomicBomb()
{
}

void AtomicBomb::Draw()
{
	glPushMatrix();
	if (!menuBomb)
	{
		glTranslatef(x, (time * 10) + 1, z);
		glRotated(-90, 0, 1, 0);
	}
	else {
		glTranslatef(x, 0, z);
	}


	glPushMatrix(); //Esfera da bomba
	glColor3f(0, 0, 0);
	GLUquadric *qobj = gluNewQuadric();
	gluSphere(qobj, 1, 20, 20);
	gluDeleteQuadric(qobj);
	glPopMatrix();

	glPushMatrix(); //Corpo da bomba
	glColor3f(1, 1, 1);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	GLUquadric* cil = gluNewQuadric();
	glRotated(-90, 0, 1, 0);
	glRotated(-90, 1, 0, 0);
	gluQuadricTexture(cil, GL_TRUE);
	gluCylinder(cil, 1, 1, 2, 10, 10);
	glPopMatrix();

	glPushMatrix(); //Tapa corpo bomba
	glRotated(-90, 1, 0, 0);
	glTranslatef(0, 0, 2);
	GLUquadric* disk = gluNewQuadric();
	gluDisk(disk, 0, 1, 10, 10);
	glPopMatrix();

	glPushMatrix(); //Cauda Bomba
	glColor3f(0, 0, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	GLUquadric* cil2 = gluNewQuadric();
	glRotated(-90, 1, 0, 0);
	glTranslatef(0, 0, 2);
	gluCylinder(cil2, 0.5, 0.5, 1, 10, 10);
	glPopMatrix();

	glPushMatrix(); //Tapa Cauda Bomba
	glColor3f(0, 0, 0);
	glRotated(-90, 1, 0, 0);
	glTranslatef(0, 0, 3);
	GLUquadric* disk1 = gluNewQuadric();
	gluDisk(disk1, 0, 1, 10, 10);
	glPopMatrix();

	glPushMatrix(); //Cauda Final Bomba
	glColor3f(0, 0, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	GLUquadric* cil3 = gluNewQuadric();
	glRotated(-90, 1, 0, 0);
	glTranslatef(0, 0, 3);
	gluCylinder(cil3, 1, 1, 1, 10, 10);
	glPopMatrix();

	glColor3f(1, 1, 1);
	glPopMatrix();
}

std::vector<GLint*> AtomicBomb::Explode()
{
	GLint xBomba = x + (TAMANHO_MAPA) / 2;
	GLint zBomba = z + (TAMANHO_MAPA) / 2;
	xBomba = xBomba / 4;
	zBomba = zBomba / 4;

	std::vector<GLint*> blocksToDestroy;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[0][0] = xBomba;
	blocksToDestroy[0][1] = zBomba;

	int c;
	for (int i = 1; i < 6; i++)
	{
		c = (i - 1) * 4 + 1;
		for (int j = 0; j < 4; j++)
		{
			blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
		}

		blocksToDestroy[c][0] = xBomba + i;
		blocksToDestroy[c][1] = zBomba - i;
		blocksToDestroy[c + 1][0] = xBomba + i;
		blocksToDestroy[c + 1][1] = zBomba + i;
		blocksToDestroy[c + 2][0] = xBomba - i;
		blocksToDestroy[c + 2][1] = zBomba + i;
		blocksToDestroy[c + 3][0] = xBomba - i;
		blocksToDestroy[c + 3][1] = zBomba - i;
	}

	for (unsigned i = 0; i < (unsigned)25; i++)
	{
		blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
		blocksToDestroy[i + c + 4][0] = i;
		blocksToDestroy[i + c + 4][1] = zBomba;
	}

	for (unsigned i = 0; i < (unsigned)25; i++)
	{
		blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
		blocksToDestroy[i + c + 29][0] = xBomba;
		blocksToDestroy[i + c + 29][1] = i;
	}

	return blocksToDestroy;
}
