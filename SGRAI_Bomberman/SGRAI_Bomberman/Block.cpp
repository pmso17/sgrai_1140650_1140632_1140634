#include "stdafx.h"
#include "Block.h"


Block::Block(GLint xCoord, GLint zCoord,GLboolean destructible)
{
	x = (xCoord*TAMANHO_BLOCK);
	y = TAMANHO_BLOCK / 2;
	z = (zCoord*TAMANHO_BLOCK);
	isDestructible = destructible;
	partBuilder = new PartBuilder();
}


Block::~Block()
{
}

GLint Block::GetTamanhoBlock()
{
	return TAMANHO_BLOCK;
}

void Block::Draw(GLint texture) {
	GLint* textures = new GLint[5];
	for (int i = 0; i < 5; i++)
	{
		textures[i] = texture;
	}

	glPushMatrix();
	glTranslatef(x,y,z);
	glScalef(TAMANHO_BLOCK, TAMANHO_BLOCK, TAMANHO_BLOCK);
	partBuilder->drawCube(textures);
	glPopMatrix();
}

GLint * Block::getPosition()
{
	GLint* pos = new GLint[2];
	pos[0] = x;
	pos[1] = z;
	return pos;
}