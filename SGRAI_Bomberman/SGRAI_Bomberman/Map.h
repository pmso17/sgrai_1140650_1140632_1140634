#pragma once
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include <iostream>
#include <vector>
#include "GL/glut.h"
#include "SOIL.h"
#include "Game1vsPC.h"
#include "Game1vs1.h"
#include "Bomb.h"
#include "Block.h"
#include "Player.h"
#include "EndOfGame.h"
#include "Explosion.h"
#include "HorizontalBomb.h"
#include "RandomBomb.h"
#include "CrossBomb.h"
#include "BoxBomb.h"
#include "VerticalBomb.h"
#include "AtomicBomb.h"
#include "Sound.h"

#define COLISION_BOX_DIMENSION 0.8
#define TAMANHO_MAPA 100
#define NOME_TEXTURA_CHAO "Data/chao.png"
class Map
{
public:
	Map(GLint* texture, std::vector<Player*> listPlayers,Sound* music,GLint mode); //0 vsPC 1 vs1
	~Map();
	void clearMatrix(GLint razao);
	void Draw();
	GLboolean detetaColisoes(Player* player);
	void SpawnBomba(Player* player);
	int LoadGLTextures();
	void checkForExplosions(Player* player1, Player* player2);
	void checkForPowerUps(Player* player1, Player* player2);
	Bomb * createSpecificBomb(int type, int x, int z);
	void givePlayerLocation(Player* pc, int* poz);
	int validateBlock(int* block);
	char mapaDanger[25][25];
	std::vector<Player*> listPlayers;
private:
	GLint deathMode;
	GLfloat texCoords[4][2] = {
		{ 0.0f, 0.0f },  // Canto Inferior Esquerdo
		{ 10.0f, 0.0f },  // Canto Inferior Direito
		{ 10.0f, 10.0f },   // Canto Superior Direito
		{ 0.0f, 10.0f }  // Canto Superior Esquerdo
	};
	GLint displayListMapa;
	GLint texture[1];
	GLint* textureForPlayer;
	GLint mapa[25][25]; //0 nada  1 bloco metal  2 bloco madeira  3 bomba
	std::vector<Bomb*>  listBombas;
	std::vector<Block*> listBlocks; //25+25+24+24=98 � volta  11*11=121 centrais  218 blocos stone  50 blocos madeira
	std::vector<Explosion*> listExplosions;
	std::vector<Bomb*> listPowerUps;
	Sound* musica;
	Sound* explosao;
	void createBlocksBox(GLint razao);
	void createBlocksCentral(GLint razao);
	void createStoneBlocks(GLint razao);
	void createWoodBlocks(GLint razao);
	void DrawMap();
	void DrawBlocks();
	void CreateDisplayList();
	void checkExploded(std::vector<GLint*> blocos);
	void Map::spawnPowerUp(GLint * block);
	void destroyBlock(GLint* bloco);
	void checkDeaths(std::vector<GLint*> blocosAfetados, Player * player1, Player * player2);
	void createExplosions(std::vector<GLint*> blocos);
};



