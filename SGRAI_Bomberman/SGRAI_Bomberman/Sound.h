#pragma once
#include <AL/alut.h>
#include "Configurations.h"
class Sound
{
private:
	ALuint		buffer;
	ALuint		source;
	char*		path;
	int			type;//0 music 1 sound effect
public:
	Sound(char* caminho,int typeSound);
	~Sound();
	void Update();
	void InitAudio(bool loop);
	void StopAudio();
};

