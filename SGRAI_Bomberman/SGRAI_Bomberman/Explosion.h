#pragma once
#include <stdlib.h>
#include "GL/glut.h"
#include "ParticlesGenerator.h"
class Explosion
{
public:
	Explosion(GLfloat posX, GLfloat posZ);
	~Explosion();
	GLboolean Draw();
private:
	GLfloat time;
	GLfloat x, z;
	ParticlesGenerator* particle;
};

