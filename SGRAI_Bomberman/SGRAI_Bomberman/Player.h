#pragma once
#include "GL/glut.h"
#include "PartBuilder.h"
class Player
{
	typedef struct {
		float rotacaoHorizontal;
		float rotacaoVertical;
		float rotacaoVertical1;//Inclinação
	}Posicao;

	PartBuilder* partBuilder;

	Posicao posicaoCabeca;
	Posicao posicaoCorpo;
	Posicao posicaoBracoEsquerdo;
	Posicao posicaoBracoDireito;
	Posicao posicaoPernaEsquerda;
	Posicao posicaoPernaDireita;

	GLfloat x;
	GLfloat y;
	GLfloat z;
	GLfloat rotation;

	int xBombaColocada;
	int zBombaColocada;

	GLint frameAndar;
	GLboolean frameSentido;
public:
	Player(bool mode);
	Player(bool mode,GLfloat xPos,GLfloat zPos);
	~Player();
    bool inBomb;
	float* getPosicao();
	void Draw(GLint* textures);
	void Andar();
	void PrepareVictoryDance();
	void VictoryDance();
	void Move(GLfloat distance, GLfloat rotacao);
	void setPosicaoCabeca(float x, float y, float z);
	void setPosicaoCorpo(float x, float y, float z);
	void setPosicaoBracoEsquerdo(float x, float y, float z);
	void setPosicaoBracoDireito(float x, float y, float z);
	void setPosicaoPernaEsquerda(float x, float y, float z);
	void setPosicaoPernaDireita(float x, float y, float z);
	void setBombaColocada(int x, int z);
	int* getBombaColocada();
	int bombType;
private:
	void init(GLboolean mode);
	void rotacoes(Posicao parte);
	void DrawCabeca(GLint * textures);
	void DrawCorpo(GLint * textures);
	void DrawBracoEsquerdo(GLint * textures);
	void DrawBracoDireito(GLint * textures);
	void DrawPernaEsquerda(GLint * textures);
	void DrawPernaDireita(GLint * textures);
};

