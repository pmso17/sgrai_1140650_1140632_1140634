#include "stdafx.h"
#include "Sound.h"


Sound::Sound(char* caminho, int typeSound)
{
	type = typeSound;
	path = caminho;
	alutInit(0, NULL);
}



Sound::~Sound()
{
}

void Sound::Update() {
	switch (type)
	{
	case 0:
		alSourcef(source, AL_GAIN, Configurations::getVolumeMusic());
		break;
	case 1:
		alSourcef(source, AL_GAIN, Configurations::getVolumeSound());
		break;
	default:
		alSourcef(source, AL_GAIN, 1.0f);
		break;
	}
}

void Sound::InitAudio(bool loop) {
	buffer = alutCreateBufferFromFile(path);
	alGenSources(1, &source);
	alSourcei(source, AL_BUFFER, buffer);
	if (loop)
	{
		alSourcei(source, AL_LOOPING, 1);
	}
	switch (type)
	{
	case 0:
		alSourcef(source, AL_GAIN, Configurations::getVolumeMusic());
		break;
	case 1:
		alSourcef(source, AL_GAIN, Configurations::getVolumeSound());
		break;
	default:
		alSourcef(source, AL_GAIN, 1.0f);
		break;
	}
	alSourcePlay(source);
}

void Sound::StopAudio() {
	alSourceStop(source);
}
