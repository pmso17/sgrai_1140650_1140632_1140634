#pragma once
#include <stdlib.h>
#include "GL/glut.h"
#include <math.h>
#include "Configurations.h"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif
#define dtr(x)   (M_PI*(x)/180)

class Camara
{
public:
	Camara();
	~Camara();
	void changeCamera();
	void setCamera(GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara,float* posPlayer);
private:
	GLfloat x;
	GLfloat y;
	GLfloat z;
	GLboolean modoCamera;
};

