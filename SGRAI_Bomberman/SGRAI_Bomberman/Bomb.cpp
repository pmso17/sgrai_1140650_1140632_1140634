#include "stdafx.h"
#include "Bomb.h"


Bomb::Bomb(GLint xPos, GLint zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/bomb+.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	bombType = -1;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	particleGenerator = new ParticlesGenerator();
	x = (xPos - 12) * 4;
	z = (zPos - 12) * 4;
}


Bomb::~Bomb()
{
}

void Bomb::Draw()
{
	glPushMatrix();
	glTranslatef(x, 1, z);
	if (!menuBomb)
	{
		glRotated(-90, 0, 1, 0);
	}
	glColor3f(1, 1, 1);


	GLUquadric *qobj = gluNewQuadric();
	gluQuadricTexture(qobj, GL_TRUE);

	glBindTexture(GL_TEXTURE_2D, texture[0]);

	gluSphere(qobj, 1, 20, 20);
	gluDeleteQuadric(qobj);
	GLUquadric* cil = gluNewQuadric();
	glTranslatef(0, 1, 0);

	if (isBomb)
	{
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);
		glRotated(-90, 1, 0, 0);
		gluCylinder(cil, 0.1, 0.1, time, 10, 10);
		gluDeleteQuadric(cil);
		glPopMatrix();
		glTranslatef(0, time, 0);
		particleGenerator->UpdateParticles();
		particleGenerator->Draw();
	}

	glPopMatrix();
}

GLint * Bomb::getPosition()
{
	GLint* pos = new GLint[2];
	pos[0] = x;
	pos[1] = z;
	return pos;
}

void Bomb::setAsPowerUp() {
	isBomb = false;
}

bool Bomb::checkExplode()
{
	if (time > 0)
	{
		time -= 0.01;
		return GL_FALSE;
	}
	return GL_TRUE;
}

std::vector<GLint*> Bomb::Explode()
{
	GLint xBomba = x + (TAMANHO_MAPA) / 2;
	GLint zBomba = z + (TAMANHO_MAPA) / 2;
	xBomba = xBomba / 4;
	zBomba = zBomba / 4;

	std::vector<GLint*> blocksToDestroy;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[0][0] = xBomba;
	blocksToDestroy[0][1] = zBomba;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[1][0] = xBomba + 1;
	blocksToDestroy[1][1] = zBomba;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[2][0] = xBomba - 1;
	blocksToDestroy[2][1] = zBomba;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[3][0] = xBomba;
	blocksToDestroy[3][1] = zBomba + 1;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[4][0] = xBomba;
	blocksToDestroy[4][1] = zBomba - 1;
	return blocksToDestroy;
}

void Bomb::setMenuBomb()
{
	menuBomb = true;
}
