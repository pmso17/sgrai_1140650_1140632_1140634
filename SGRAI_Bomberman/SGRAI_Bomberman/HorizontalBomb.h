#pragma once
#include "Bomb.h"
class HorizontalBomb: public Bomb
{
public:
	HorizontalBomb(GLint xPos,GLint zPos);
	~HorizontalBomb();
	std::vector<GLint*> Explode() override;
};

