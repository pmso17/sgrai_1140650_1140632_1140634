#include "stdafx.h"
#include "Game1vs1Controller.h"


Game1vs1Controller::Game1vs1Controller(Sound* music)
{
	configs = readConfigFile();
	LoadGLTextures(Configurations::getTextureNumber1());
	player1 = new Player(true, -44, -44);
	player2 = new Player(true, 44, 44);
	std::vector<Player*> list;
	list.insert(list.end(), player1);
	list.insert(list.end(), player2);
	mapa = new Map(texture, list, music,1);
	camara = new Camara();
}


Game1vs1Controller::~Game1vs1Controller()
{
}

GLint* Game1vs1Controller::getTexture() {
	return texture;
}

void Game1vs1Controller::Draw(GLfloat walking1, GLfloat rotacaoPlayer1,
	GLfloat walking2, GLfloat rotacaoPlayer2,
	GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara)
{
	float* posPlayer = NULL;
	switch (idCamara) {
	case 0:
		posPlayer = new float[3];
		posPlayer[0] = 0;
		posPlayer[1] = 3.6;
		posPlayer[2] = 0;
		camara->setCamera(rotacaoCamaraHorizontal, rotacaoCamaraVertical, raioCamara, posPlayer);
		break;
	case 1:
		posPlayer = player1->getPosicao();
		camara->setCamera(rotacaoCamaraHorizontal, rotacaoCamaraVertical, raioCamara, posPlayer);
		break;
	case 2:
		posPlayer = player2->getPosicao();
		camara->setCamera(rotacaoCamaraHorizontal, rotacaoCamaraVertical, raioCamara, posPlayer);
		break;
	}

	mapa->checkForExplosions(player1, player2);
	mapa->checkForPowerUps(player1, player2);
	mapa->Draw();

	if (walking1 != 0)
	{
		player1->Andar();
	}

	player1->Move(walking1, rotacaoPlayer1);
	if (mapa->detetaColisoes(player1))
	{
		player1->Move(-walking1, rotacaoPlayer1);
	}

	if (walking2 != 0)
	{
		player2->Andar();
	}

	player2->Move(walking2, rotacaoPlayer2);
	if (mapa->detetaColisoes(player2))
	{
		player2->Move(-walking2, rotacaoPlayer2);
	}

	player1->Draw(texture);
	player2->Draw(texture);
}

void Game1vs1Controller::ColocarBomba(GLint who)
{
	Player* player = NULL;
	switch (who) {
	case 1:
		player = player1;
		break;
	case 2:
		player = player2;
		break;
	}
	float* posPlayer = player->getPosicao();
	mapa->SpawnBomba(player);

}

void Game1vs1Controller::ChangeCamera()
{
	idCamara++;
	if (idCamara>2)
	{
		idCamara = 0;
	}
}

void Game1vs1Controller::LoadGLTexturesHead(GLint position) {

	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_front.png");
	texture[0] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_back.png");
	texture[1] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_lateral.png");
	texture[2] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_up.png");
	texture[3] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_down.png");
	texture[4] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vs1Controller::LoadGLTexturesBody(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_front.png");
	texture[5] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_back.png");
	texture[6] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_lateral.png");
	texture[7] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_up.png");
	texture[8] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_down.png");
	texture[9] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vs1Controller::LoadGLTexturesArm(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_front.png");
	texture[10] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_back.png");
	texture[11] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_lateral.png");
	texture[12] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_up.png");
	texture[13] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_down.png");
	texture[14] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vs1Controller::LoadGLTexturesLeg(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_front.png");
	texture[15] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_back.png");
	texture[16] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_lateral.png");
	texture[17] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_up.png");
	texture[18] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_down.png");
	texture[19] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

int Game1vs1Controller::LoadGLTextures(GLint position)                                    // Load Bitmaps And Convert To Textures
{
	/* load an image file directly as a new OpenGL texture */
	LoadGLTexturesHead(position);
	LoadGLTexturesBody(position);
	LoadGLTexturesArm(position);
	LoadGLTexturesLeg(position);

	for (int i = 0; i < 20; i++)
	{
		if (texture[i] == 0) {
			return false;
		}
	}


	// Typical Texture Generation Using Data From The Bitmap
	for (int i = 0; i < 20; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	return true;
}
