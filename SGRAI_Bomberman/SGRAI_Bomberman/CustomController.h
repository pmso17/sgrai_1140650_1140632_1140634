#pragma once
#include <string.h>
#include <stdlib.h>
#include "Player.h"
#include "ReaderWriterCSV.h"
#include "GL/glut.h"
#include "SOIL.h"

class CustomController
{
public:

	CustomController();
	~CustomController();
	GLint getActualConfiguration();
	void LoadGLTexturesHead(GLint position);
	void Draw();
	void nextConfiguration();
	void previousConfiguration();
	void Andar();
private:
	int qtdConfigurations;
	int actualConfiguration;
	Player* player;
	GLint texture[21];
	configStruct* configs;
	void LoadGLTexturesBody(GLint position);
	void LoadGLTexturesArm(GLint position);
	void LoadGLTexturesLeg(GLint position);
	int LoadGLTextures(GLint position);
};
