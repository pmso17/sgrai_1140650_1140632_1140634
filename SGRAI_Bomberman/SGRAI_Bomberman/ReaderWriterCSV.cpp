#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <fstream>
#include "ConfigStruct.h"

#define CONFIG_FILE "config.cfg"

using namespace std;

int getLinhas() {
	std::ifstream inFile(CONFIG_FILE);
	return std::count(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), '\n') + 1;
}

configStruct* readConfigFile() {
	int qtdLinhas = getLinhas();

	configStruct* configList = new configStruct[qtdLinhas];
	FILE* f;
	fopen_s(&f,CONFIG_FILE, "r");

	char line[255];
	size_t len = 0;

	if (f == NULL) {
		printf("Caminho inv�lido\n");
		return NULL;
	}

	int i = 0;
	while (fgets(line, 50, f)!=NULL) {
		char delim1 = ';';
		char delim2 = ',';
		char* linha;

		//strtok_s(line, &delim1,&linha);
		strtok_s(line, "\n",&linha);
		strcpy_s(configList[i].pastaSkins,50,line);
		i++;
		/*char* partes;
		strtok_s(linha, &delim2,&partes);
		strcpy_s(configList[i].tipoCabeca,50,linha);
		strtok_s(partes, &delim2, &linha);
		strcpy_s(configList[i].tipoCorpo,50,partes);
		strtok_s(linha, &delim2, &partes);
		strcpy_s(configList[i].tipoBracos,50,linha);
		strcpy_s(configList[i].tipoPernas,50,partes);*/
	}

	fclose(f);
	return configList;
}
