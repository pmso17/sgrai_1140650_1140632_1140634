#include "stdafx.h"
#include "ParticlesGenerator.h"


ParticlesGenerator::ParticlesGenerator()
{
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		listParticles[i] = {};
		listParticles[i].position[0] = 0; //x
		listParticles[i].position[1] = 0; //y
		listParticles[i].position[2] = 0; //z
	}
}


ParticlesGenerator::~ParticlesGenerator()
{
}


void ParticlesGenerator::Draw() {
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		glPushMatrix();
		glColor3f(listParticles[i].color[0], listParticles[i].color[1], listParticles[i].color[2]);
		glTranslatef(listParticles[i].position[0], listParticles[i].position[1], listParticles[i].position[2]);
		glutSolidSphere(0.05, 10, 10);
		glPopMatrix();
	}
	if (!PARTY_MODE)
	{
		glColor3f(1, 1, 1);
	}
}

void ParticlesGenerator::UpdateParticles() {
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		listParticles[i].position[0] = (rand() % 19 + (-9))*0.03; //x
		listParticles[i].position[1] = (rand() % 20)*0.03; //x
		listParticles[i].position[2] = (rand() % 19 + (-9))*0.03; //x
		int randColor = rand() % 4;
		switch (randColor) {
		case 0://red
			listParticles[i].color[0] = 1.0f;
			listParticles[i].color[1] = 0.25f;
			listParticles[i].color[2] = 0.0f;
			break;
		case 1://yellow
			listParticles[i].color[0] = 1.0f;
			listParticles[i].color[1] = 0.9f;
			listParticles[i].color[2] = 0.0f;
			break;
		case 2://light yellow
			listParticles[i].color[0] = 1.0f;
			listParticles[i].color[1] = 1.0f;
			listParticles[i].color[2] = 0.0f;
			break;
		case 3://gold maybe?
			listParticles[i].color[0] = 1.0f;
			listParticles[i].color[1] = 0.95f;
			listParticles[i].color[2] = 0.8f;
			break;
		}
	}
}