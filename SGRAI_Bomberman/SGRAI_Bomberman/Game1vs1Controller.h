#pragma once
#include <stdlib.h>
#include "GL/glut.h"
#include "ReaderWriterCSV.h"
#include "ConfigStruct.h"
#include "Map.h"
#include "Player.h"
#include "Camara.h"
#include "Configurations.h"

class Game1vs1Controller
{
public:
	Game1vs1Controller(Sound* music);
	~Game1vs1Controller();
	GLint * getTexture();
	void Draw(GLfloat walking1, GLfloat rotacaoPlayer1,GLfloat walking2, GLfloat rotacaoPlayer2,GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara);
	void ColocarBomba(GLint who);
	void ChangeCamera();
private:
	GLint idCamara;
	GLint texture[21];
	configStruct* configs;
	Map* mapa;
	Player* player1;
	Player* player2;
	Camara* camara;//0 Camara geral 1 camara player1  2 camara player2
	void LoadGLTexturesHead(GLint position);
	void LoadGLTexturesBody(GLint position);
	void LoadGLTexturesArm(GLint position);
	void LoadGLTexturesLeg(GLint position);
	int LoadGLTextures(GLint position);
};

