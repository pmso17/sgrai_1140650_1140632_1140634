// Aula5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include "Sound.h"
#include "SOIL.h"
#include "GameSelector.h"
#include "Custom.h"
#include "ReaderWriterCSV.h"
#include "ConfigStruct.h"
#include "Bomb.h"
#include "HorizontalBomb.h"
#include "VerticalBomb.h"
#include "CrossBomb.h"
#include "BoxBomb.h"
#include "AtomicBomb.h"
#include "RandomBomb.h"

using namespace std;

#define DEBUG 1
#define FPS 24
#define NOME_TEXTURA_MENU_BG "Data/bg.png"
#define NOME_TEXTURA_MENU_BUTTON_PLAY "Data/play.png"
#define NOME_TEXTURA_MENU_BUTTON_CUSTOM "Data/custom.png"
#define NOME_TEXTURA_MENU_BUTTON_QUIT "Data/quit.png"


/* VARIAVEIS GLOBAIS */

typedef struct teclas_t {
	GLboolean   w, s, enter;
}Teclas;

typedef struct {
	Sound*		music;
	GLboolean   doubleBuffer;
	GLboolean	timerFlag;
	GLint       delayMovimento;
	GLboolean   debug;
	teclas_t	teclas;
	GLint		selectedValue;
	Bomb*		selectionBomb;
	GLfloat     ratioWidth, ratioHeight;
}Estado;

Estado estado;

typedef struct {
	string text;
	GLfloat minX;
	GLfloat maxX;
	GLfloat minY;
	GLfloat maxY;
}Button;

Button *botoes = new Button[3];

GLfloat texCoords[][2] = {
	{0.0f, 0.0f},  // Canto Inferior Esquerdo
	{1.0f, 0.0f },  // Canto Inferior Direito
	{1.0f, 1.0f},   // Canto Superior Direito
	{0.0f, 1.0f}  // Canto Superior Esquerdo
};

GLint textur[4];

void selectOption(GLint selectValue) {
	estado.music->StopAudio();
	estado.timerFlag = GL_FALSE;
	switch (selectValue)
	{
	case 0:
		printf("PLAY\n");
		GameSelector::Init();
		break;
	case 1:
		printf("CUSTOM\n");
		Custom::Init();
		break;
	case 2:
		exit(1);
		break;
	default:
		printf("Error: %d\n", selectValue);
		break;
	}
}


int LoadGLTextures()                                    
{
	/* load an image file directly as a new OpenGL texture */
	textur[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	textur[1] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BUTTON_PLAY, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	textur[2] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BUTTON_CUSTOM, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	textur[3] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BUTTON_QUIT, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

	if (textur[0] == 0 || textur[1] == 0 || textur[2] == 0 || textur[3] == 0) {
		return false;
	}


	// Typical Texture Generation Using Data From The Bitmap
	glBindTexture(GL_TEXTURE_2D, textur[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, textur[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, textur[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, textur[3]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return true;
}

void selectMenuBomb() {
	int x = rand() % 7;
	switch (x) {
	case 0:
		estado.selectionBomb = new HorizontalBomb(0,0);
		break;
	case 1:
		estado.selectionBomb = new RandomBomb(0,0);
		break;
	case 2:
		estado.selectionBomb = new CrossBomb(0,0);
		break;
	case 3:
		estado.selectionBomb = new VerticalBomb(0,0);
		break;
	case 4:
		estado.selectionBomb = new BoxBomb(0,0);
		break;
	case 5:
		estado.selectionBomb = new AtomicBomb(0, 0);
	default:
		estado.selectionBomb = new Bomb(0,0);
		break;
	}
	estado.selectionBomb->setMenuBomb();
}

/* Inicializa��o do ambiente OPENGL */
void inicia_modelo()
{
	estado.timerFlag = GL_TRUE;
	estado.selectedValue = 0;
	estado.delayMovimento = 50;
	selectMenuBomb();

	botoes[0].text = "PLAY";
	botoes[0].minX = -3.0F;
	botoes[0].maxX = 3.0F;
	botoes[0].minY = 1.0F;
	botoes[0].maxY = 2.5F;

	botoes[1].text = "CUSTOMIZATION";
	botoes[1].minX = -3.0F;
	botoes[1].maxX = 3.0F;
	botoes[1].minY = -1.0F;
	botoes[1].maxY = 0.5F;

	botoes[2].text = "QUIT";
	botoes[2].minX = -3.0F;
	botoes[2].maxX = 3.0F;
	botoes[2].minY = -3.0F;
	botoes[2].maxY = -1.5F;
}


void Init(void)
{

	estado.music = new  Sound("Data/Music/title.wav",0);
	estado.music->InitAudio(true);
	inicia_modelo();
	if (!LoadGLTextures())
	{
		printf("Error Loading Textures");
	}
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

/* CALLBACK PARA REDIMENSIONAR JANELA */
void Reshape(int width, int height)
{
	estado.ratioHeight = height / 700.0f;
	estado.ratioWidth = width / 700.0f;
	glPushMatrix();
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5, 5, -5, 5, -100, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPopMatrix();
}


/* ... definicao das rotinas auxiliares de desenho ... */

void desenhaBackGround()
{
	glBindTexture(GL_TEXTURE_2D, textur[0]);
	glBegin(GL_POLYGON);
	glTexCoord2fv(texCoords[0]);
	glVertex3f(-5, -5, 0);
	glTexCoord2fv(texCoords[1]);
	glVertex3f(5, -5, 0);
	glTexCoord2fv(texCoords[2]);
	glVertex3f(5, 5, 0);
	glTexCoord2fv(texCoords[3]);
	glVertex3f(-5, 5, 0);
	glEnd();
}

/* Desenha os bot�es */
void desenhaButton(GLfloat minX, GLfloat maxX, GLfloat minY, GLfloat maxY, GLint texNumber) {
	glBindTexture(GL_TEXTURE_2D, textur[texNumber]);
	glBegin(GL_POLYGON);
	glTexCoord2fv(texCoords[0]);
	glVertex3f(minX, minY, .1);
	glTexCoord2fv(texCoords[1]);
	glVertex3f(maxX, minY, 0.1);
	glTexCoord2fv(texCoords[2]);
	glVertex3f(maxX, maxY, 0.1);
	glTexCoord2fv(texCoords[3]);
	glVertex3f(minX, maxY, 0.1);
	glEnd();
}

/* Callback de desenho */
void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/* ... chamada das rotinas auxiliares de desenho ... */

	glPushMatrix();
	desenhaBackGround();
	desenhaButton(botoes[0].minX, botoes[0].maxX, botoes[0].minY, botoes[0].maxY, 1); //Play
	desenhaButton(botoes[1].minX, botoes[1].maxX, botoes[1].minY, botoes[1].maxY, 2); //Customization
	desenhaButton(botoes[2].minX, botoes[2].maxX, botoes[2].minY, botoes[2].maxY, 3); //Quit

	glPushMatrix();
	glTranslatef(0, 0, 20);
	glScalef(.3, .3, .3);
	glTranslated(36, 5 - (7 * estado.selectedValue), 0);
	glRotated(90, 0, 1, 0);
	estado.selectionBomb->Draw();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, 0, 20);
	glScalef(.3, .3, .3);
	glTranslated(60, 5 - (7 * estado.selectedValue), 0);
	glRotated(90, 0, 1, 0);
	estado.selectionBomb->Draw();
	glPopMatrix();

	glPopMatrix();

	if (estado.doubleBuffer)
		glutSwapBuffers();
	else
		glFlush();

}

/*******************************
***   callbacks timer   ***
*******************************/
/* Callback de temporizador */
void Timer(int value)
{
	clock_t drawStartTime = clock();
	glutPostRedisplay();
	clock_t drawEndTime = clock();

	int delayToNextFrame = (CLOCKS_PER_SEC / FPS) - (drawEndTime - drawStartTime);
	delayToNextFrame = floor(delayToNextFrame + 0.5);
	delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;

	if (estado.timerFlag)
	{
		glutTimerFunc(delayToNextFrame, Timer, 0);
	}
	/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
	somente os valores das vari�veis ... */

	// alterar o modelo.theta[] usando a vari�vel modelo.eixoRodar como indice

	/* redesenhar o ecr� */
	glutPostRedisplay();
}


/*******************************
***  callbacks de teclado    ***
*******************************/



/* Callback para interaccao via teclado (carregar na tecla) */
void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(1);
		/* ... accoes sobre outras teclas ... */
		break;
	}

	if (DEBUG) {
		printf("Carregou na tecla %c\n", key);
	}
}

/* Callback para interaccao via teclado (largar a tecla) */
void KeyUp(unsigned char key, int x, int y)
{

	switch (key)
	{
	case 'w':
	case 'W':
		if (estado.selectedValue > 0)
		{
			estado.selectedValue--;
		}
		break;
	case's':
	case'S':
		if (estado.selectedValue < 2)
		{
			estado.selectedValue++;
		}
		break;
	case 13:
	case ' ':
		selectOption(estado.selectedValue);
		break;
	}

	if (DEBUG)
		printf("Largou a tecla %d\n", key);
}

/* Callback para interaccao via teclas especiais  (carregar na tecla) */
void SpecialKey(int key, int x, int y)
{
	/* ... accoes sobre outras teclas especiais ...
	GLUT_KEY_F1 ... GLUT_KEY_F12
	GLUT_KEY_UP
	GLUT_KEY_DOWN
	GLUT_KEY_LEFT
	GLUT_KEY_RIGHT
	GLUT_KEY_PAGE_UP
	GLUT_KEY_PAGE_DOWN
	GLUT_KEY_HOME
	GLUT_KEY_END
	GLUT_KEY_INSERT
	*/

	if (DEBUG)
		printf("Carregou na tecla especial %d\n", key);
}

/* Callback para interaccao via teclas especiais (largar na tecla) */
void SpecialKeyUp(int key, int x, int y)
{

	if (DEBUG)
		printf("Largou a tecla especial %d\n", key);

}

/*******************************
***  callbacks do rato       ***
*******************************/

void MouseMotion(int x, int y)
{
	/* x,y    => coordenadas do ponteiro quando se move no rato
	a carregar em teclas
	*/

	if (DEBUG)
		printf("Mouse Motion %d %d\n", x, y);

}

void MousePassiveMotion(int x, int y)
{
	/* x,y    => coordenadas do ponteiro quando se move no rato
	sem estar a carregar em teclas
	*/
	if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>175 * estado.ratioHeight && y < 280 * estado.ratioHeight)//PLAY
	{
		estado.selectedValue = 0;
	}
	if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>315 * estado.ratioHeight && y < 420 * estado.ratioHeight)//CUSTOM
	{
		estado.selectedValue = 1;
	}
	if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>455 * estado.ratioHeight && y < 560 * estado.ratioHeight)//QUIT
	{
		estado.selectedValue = 2;
	}

	/*if (DEBUG)
		printf("Mouse Passive Motion %d %d\n", x, y);*/

}

void Mouse(int button, int state, int x, int y)
{
	/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
	state  => GLUT_UP, GLUT_DOWN
	x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
	*/

	if (state == GLUT_UP)
	{
		switch (button) {
		case GLUT_LEFT_BUTTON:
			printf("x=%d y=%d\n", x, y);
			if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>175 * estado.ratioHeight && y < 280 * estado.ratioHeight)//PLAY
			{
				selectOption(0);
			}
			if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>315 * estado.ratioHeight && y < 420 * estado.ratioHeight)//CUSTOM
			{
				selectOption(1);
			}
			if (x > 140 * estado.ratioWidth && x < 560 * estado.ratioWidth && y>455 * estado.ratioHeight && y < 560 * estado.ratioHeight)//QUIT
			{
				selectOption(2);
			}
			break;
		case GLUT_MIDDLE_BUTTON:

			break;
		case GLUT_RIGHT_BUTTON:

			break;
		}
	}
	if (DEBUG)
		printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
}


void MainInit() {
	glClearColor(0, 0, 0, 0);
	Init();
	/* Registar callbacks do GLUT */

	/* callbacks de janelas/desenho */
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	/* Callbacks de teclado */
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	//glutSpecialUpFunc(SpecialKeyUp);

	/* callbacks rato */
	glutPassiveMotionFunc(MousePassiveMotion);
	//glutMotionFunc(MouseMotion);
	glutMouseFunc(Mouse);

	/* callbacks timer/idle */
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	//glutIdleFunc(Idle);
}

int main(int argc, char **argv)
{
	char str[] = " makefile MAKEFILE Makefile ";

	estado.doubleBuffer = 1;
	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(700, 700);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Bomberman 3D") == GL_FALSE)
		exit(1);
	MainInit();

	/* COMECAR... */
	glutMainLoop();
	return 0;
}