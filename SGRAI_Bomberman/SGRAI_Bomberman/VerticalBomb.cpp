#include "stdafx.h"
#include "VerticalBomb.h"


VerticalBomb::VerticalBomb(GLint xPos, GLint zPos):Bomb(xPos,zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/bombCol.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	bombType = 3;
}


VerticalBomb::~VerticalBomb()
{
}

std::vector<GLint*> VerticalBomb::Explode()
{
	GLint xBomba = x + (TAMANHO_MAPA) / 2;
	GLint zBomba = z + (TAMANHO_MAPA) / 2;
	xBomba = xBomba / 4;
	zBomba = zBomba / 4;

	std::vector<GLint*> blocksToDestroy;

	for (unsigned i = 0; i < 24; i++)
	{
		blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
		blocksToDestroy[i][0] = xBomba;
		blocksToDestroy[i][1] = i;
	}

	return blocksToDestroy;
}