#include "stdafx.h"
#include "BoxBomb.h"

BoxBomb::BoxBomb(GLint xPos, GLint zPos) :Bomb(xPos, zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/bombBox.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	bombType = 4;
}


BoxBomb::~BoxBomb()
{
}


std::vector<GLint*> BoxBomb::Explode()
{
	GLint xBomba = x + (TAMANHO_MAPA) / 2;
	GLint zBomba = z + (TAMANHO_MAPA) / 2;
	xBomba = xBomba / 4;
	zBomba = zBomba / 4;

	std::vector<GLint*> blocksToDestroy;


	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[0][0] = xBomba + 1;
	blocksToDestroy[0][1] = zBomba;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[1][0] = xBomba - 1;
	blocksToDestroy[1][1] = zBomba;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[2][0] = xBomba;
	blocksToDestroy[2][1] = zBomba + 1;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[3][0] = xBomba;
	blocksToDestroy[3][1] = zBomba - 1;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[4][0] = xBomba - 1;
	blocksToDestroy[4][1] = zBomba - 1;
	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[5][0] = xBomba - 1;
	blocksToDestroy[5][1] = zBomba + 1;
	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[6][0] = xBomba + 1;
	blocksToDestroy[6][1] = zBomba - 1;
	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[7][0] = xBomba + 1;
	blocksToDestroy[7][1] = zBomba + 1;

	return blocksToDestroy;
}