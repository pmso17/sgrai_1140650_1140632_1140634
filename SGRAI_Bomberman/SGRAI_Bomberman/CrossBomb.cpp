#include "stdafx.h"
#include "CrossBomb.h"


CrossBomb::CrossBomb(GLint xPos, GLint zPos) :Bomb(xPos, zPos)
{
	texture[0] = SOIL_load_OGL_texture("Data/bombCross.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	bombType = 2;
}


CrossBomb::~CrossBomb()
{
}

std::vector<GLint*> CrossBomb::Explode()
{
	GLint xBomba = x + (TAMANHO_MAPA) / 2;
	GLint zBomba = z + (TAMANHO_MAPA) / 2;
	xBomba = xBomba / 4;
	zBomba = zBomba / 4;

	std::vector<GLint*> blocksToDestroy;

	blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
	blocksToDestroy[0][0] = xBomba;
	blocksToDestroy[0][1] = zBomba;

	for (int i = 1; i < 6; i++)
	{
		int c = (i - 1) * 4 + 1;
		for (int j = 0; j < 4; j++)
		{
			blocksToDestroy.insert(blocksToDestroy.end(), new GLint[2]);
		}

		blocksToDestroy[c][0] = xBomba + i;
		blocksToDestroy[c][1] = zBomba - i;
		blocksToDestroy[c + 1][0] = xBomba + i;
		blocksToDestroy[c + 1][1] = zBomba + i;
		blocksToDestroy[c + 2][0] = xBomba - i;
		blocksToDestroy[c + 2][1] = zBomba + i;
		blocksToDestroy[c + 3][0] = xBomba - i;
		blocksToDestroy[c + 3][1] = zBomba - i;
	}

	return blocksToDestroy;
}
