#include "stdafx.h"
#include "CustomController.h"



CustomController::CustomController()
{
	player = new Player(false);
	qtdConfigurations = getLinhas();
	actualConfiguration = 0;
	configs = readConfigFile();
}


CustomController::~CustomController()
{
	delete player;
}

GLint CustomController::getActualConfiguration() {
	return actualConfiguration;
}

void CustomController::LoadGLTexturesHead(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_front.png");
	texture[0] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_back.png");
	texture[1] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_lateral.png");
	texture[2] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_up.png");
	texture[3] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_down.png");
	texture[4] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void CustomController::LoadGLTexturesBody(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_front.png");
	texture[5] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_back.png");
	texture[6] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_lateral.png");
	texture[7] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_up.png");
	texture[8] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_down.png");
	texture[9] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void CustomController::LoadGLTexturesArm(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_front.png");
	texture[10] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_back.png");
	texture[11] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_lateral.png");
	texture[12] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_up.png");
	texture[13] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_down.png");
	texture[14] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void CustomController::LoadGLTexturesLeg(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_front.png");
	texture[15] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_back.png");
	texture[16] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_lateral.png");
	texture[17] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_up.png");
	texture[18] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_down.png");
	texture[19] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

int CustomController::LoadGLTextures(GLint position)                                    // Load Bitmaps And Convert To Textures
{
	/* load an image file directly as a new OpenGL texture */
	LoadGLTexturesHead(position);
	LoadGLTexturesBody(position);
	LoadGLTexturesArm(position);
	LoadGLTexturesLeg(position);

	for (int i = 0; i < 20; i++)
	{
		if (texture[i] == 0) {
			return false;
		}
	}


	// Typical Texture Generation Using Data From The Bitmap
	for (int i = 0; i < 20; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	return true;
}

void CustomController::nextConfiguration() {
	if (actualConfiguration < (qtdConfigurations - 1))
	{
		actualConfiguration++;
	}
}

void CustomController::previousConfiguration()
{
	if (actualConfiguration > 0)
	{
		actualConfiguration--;
	}
}

void CustomController::Andar() {
	player->Andar();
}

void CustomController::Draw() {
	LoadGLTextures(actualConfiguration);
	player->Draw(texture);
}