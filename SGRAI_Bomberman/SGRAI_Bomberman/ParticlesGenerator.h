#pragma once
#define PARTY_MODE 0
#define MAX_PARTICLES 100
#include <stdlib.h>
#include <time.h>
#include "Particle.h"
class ParticlesGenerator
{
public:
	ParticlesGenerator();
	~ParticlesGenerator();
	void Draw();
	void UpdateParticles();
private:
	particle listParticles[MAX_PARTICLES];
};

