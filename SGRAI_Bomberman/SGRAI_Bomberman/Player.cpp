#include "stdafx.h"
#include "Player.h"
#include <math.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif
#define dtr(x)   (M_PI*(x)/180)

Player::Player(bool mode)
{
	init(mode);
}

Player::Player(bool mode, GLfloat xPos, GLfloat zPos)
{
	init(mode);
	x = xPos;
	z = zPos;
}

Player::~Player()
{
	delete partBuilder;
}

void Player::init(GLboolean mode)
{
	xBombaColocada = 0;
	zBombaColocada = 0;
	inBomb = false;
	bombType = -1; // -1 - default bomb
	if (mode)
	{
		y = 3.6;
		z = -4;
	}
	partBuilder = new PartBuilder();
	setPosicaoCabeca(0, 0, 0);
	setPosicaoCorpo(0, 0, 0);
	setPosicaoBracoEsquerdo(0, 0, 15);
	setPosicaoBracoDireito(0, 0, 15);
	setPosicaoPernaEsquerda(0, 0, 0);
	setPosicaoPernaDireita(0, 0, 0);
}

float* Player::getPosicao() {
	float* posicao = new float[4];
	posicao[0] = x;
	posicao[1] = y;
	posicao[2] = z;
	posicao[3] = rotation;
	return posicao;
}

void Player::setPosicaoCabeca(float x, float y, float z) {
	posicaoCabeca.rotacaoHorizontal = x;
	posicaoCabeca.rotacaoVertical = y;
	posicaoCabeca.rotacaoVertical1 = z;
}

void Player::setPosicaoCorpo(float x, float y, float z) {
	posicaoCorpo.rotacaoHorizontal = x;
	posicaoCorpo.rotacaoVertical = y;
	posicaoCorpo.rotacaoVertical1 = z;
}

void Player::setPosicaoBracoEsquerdo(float x, float y, float z) {
	posicaoBracoEsquerdo.rotacaoHorizontal = x;
	posicaoBracoEsquerdo.rotacaoVertical = y;
	posicaoBracoEsquerdo.rotacaoVertical1 = z;
}

void Player::setPosicaoBracoDireito(float x, float y, float z) {
	posicaoBracoDireito.rotacaoHorizontal = -x;
	posicaoBracoDireito.rotacaoVertical = -y;
	posicaoBracoDireito.rotacaoVertical1 = -z;
}

void Player::setPosicaoPernaEsquerda(float x, float y, float z) {
	posicaoPernaEsquerda.rotacaoHorizontal = x;
	posicaoPernaEsquerda.rotacaoVertical = y;
	posicaoPernaEsquerda.rotacaoVertical1 = z;
}

void Player::setPosicaoPernaDireita(float x, float y, float z) {
	posicaoPernaDireita.rotacaoHorizontal = -x;
	posicaoPernaDireita.rotacaoVertical = -y;
	posicaoPernaDireita.rotacaoVertical1 = -z;
}

void Player::setBombaColocada(int x, int z)
{
	xBombaColocada = x;
	zBombaColocada = z;
	inBomb = true;
}

int* Player::getBombaColocada()
{
	int* vec = new int[2];
	vec[0] = xBombaColocada;
	vec[1] = zBombaColocada;
	return vec;
}



void Player::rotacoes(Posicao parte) {
	glRotatef(parte.rotacaoVertical, 1, 0, 0);
	glRotatef(parte.rotacaoHorizontal, 0, 1, 0);
	glRotatef(parte.rotacaoVertical1, 0, 0, 1);
}

void Player::DrawCabeca(GLint* textures) {
	glPushMatrix();
	rotacoes(posicaoCabeca);
	glTranslatef(0, 1, 0);
	partBuilder->drawCube(textures);
	glPopMatrix();
}

void Player::DrawCorpo(GLint* textures) {
	glPushMatrix();
	glTranslatef(0, -0.51, 0);
	glScalef(1, 2, 0.75);
	partBuilder->drawCube(&textures[5]);
	glPopMatrix();
}

void Player::DrawBracoEsquerdo(GLint* textures) {
	glPushMatrix();
	glTranslatef(0.7, 0.2, 0);
	rotacoes(posicaoBracoEsquerdo);
	glTranslatef(0, -0.71, 0);
	glScalef(0.5, 2, 0.5);
	partBuilder->drawCube(&textures[10]);
	glPopMatrix();
}
void Player::DrawBracoDireito(GLint* textures) {
	glPushMatrix();
	glTranslatef(-0.7, 0.2, 0);
	rotacoes(posicaoBracoDireito);
	glTranslatef(0, -0.71, 0);
	glScalef(0.5, 2, 0.5);
	partBuilder->drawCube(&textures[10]);
	glPopMatrix();
}

void Player::DrawPernaEsquerda(GLint* textures) {
	glPushMatrix();
	glTranslatef(0.3, -1.8, 0);
	rotacoes(posicaoPernaEsquerda);
	glTranslatef(0, -0.71, 0);
	glScalef(0.5, 2, 0.75);
	partBuilder->drawCube(&textures[15]);
	glPopMatrix();
}

void Player::DrawPernaDireita(GLint* textures) {
	glPushMatrix();
	glTranslatef(-0.3, -1.8, 0);
	rotacoes(posicaoPernaDireita);
	glTranslatef(0, -0.71, 0);
	glScalef(0.5, 2, 0.75);
	partBuilder->drawCube(&textures[15]);
	glPopMatrix();
}

void Player::Move(GLfloat distance, GLfloat rotacao) {
	x = x + (sin(dtr(rotacao))*distance);
	z = z + (cos(dtr(rotacao))*distance);
	rotation = rotacao;

}

void Player::Draw(GLint* textures)
{
	//Rota��o Corpo
	glPushMatrix();

	glTranslatef(x, y, z);
	glRotated(rotation, 0, 1, 0);

	rotacoes(posicaoCorpo);

	DrawCabeca(textures);
	DrawCorpo(textures);
	DrawBracoEsquerdo(textures);
	DrawBracoDireito(textures);
	DrawPernaEsquerda(textures);
	DrawPernaDireita(textures);

	glPopMatrix();
}

void Player::Andar() {

	if (frameAndar > 45)
	{
		frameSentido = GL_FALSE;
	}
	else {
		if (frameAndar < -45)
		{
			frameSentido = GL_TRUE;
		}
	}

	if (frameSentido)
	{
		frameAndar += 4;
	}
	else {
		frameAndar -= 4;
	}
	posicaoBracoEsquerdo.rotacaoVertical = frameAndar;
	posicaoBracoDireito.rotacaoVertical = -frameAndar;
	posicaoPernaEsquerda.rotacaoVertical = -frameAndar;
	posicaoPernaDireita.rotacaoVertical = frameAndar;
}

void Player::PrepareVictoryDance() {
	setPosicaoCabeca(0, 0, 0);
	setPosicaoCorpo(0, 0, 0);
	setPosicaoBracoEsquerdo(0, -180, 15);
	setPosicaoBracoDireito(0, -180, 15);
	setPosicaoPernaEsquerda(0, 0, 0);
	setPosicaoPernaDireita(0, 0, 0);
}

void Player::VictoryDance() {
	if (frameAndar > 45)
	{
		frameSentido = GL_FALSE;
	}
	else {
		if (frameAndar < 0)
		{
			frameSentido = GL_TRUE;
		}
	}

	if (frameSentido)
	{
		frameAndar += 2;
	}
	else {
		frameAndar -= 2;
	}
	posicaoBracoEsquerdo.rotacaoVertical1 = frameAndar;
	posicaoBracoDireito.rotacaoVertical1 = -frameAndar;
}
