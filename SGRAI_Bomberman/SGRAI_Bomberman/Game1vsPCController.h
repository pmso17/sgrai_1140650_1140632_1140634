#pragma once
#include "Map.h"
#include "Player.h"
#include "Camara.h"
#include "ConfigStruct.h"
#include "ReaderWriterCSV.h"
#include "Sound.h"
#include "Configurations.h"
#include "add.h"
#include "sub.h"
#include "operation.h"

#define PC_SPEED 0.1
class Game1vsPCController
{
public:
	Game1vsPCController(Sound* music);
	~Game1vsPCController();
	GLint * getTexture();
	void Draw(GLfloat walking, GLfloat rotacaoPlayer, GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara);
	int LoadGLTextures(GLint position);
	void ColocarBomba();
	void ChangeCamera();
private:
	void LoadGLTexturesHead(GLint position);
	void LoadGLTexturesBody(GLint position);
	void LoadGLTexturesArm(GLint position);
	void LoadGLTexturesLeg(GLint position);
	GLint texture[21];
	configStruct* configs;
	Map* mapa;
	Player* player;
	Player* computer;
	Camara* camara;
	int tics;
	void computerThink();
	std::vector<int*> path;
	char ac;
	void aiThinking1();
	void runFromBombs();
	char checkIfInDanger(int* pcLocation);
	bool checkPath(int i);
	bool checkRepeated(std::vector<int*>* auxPlaces, int* validate);
	void makePathTo(int* end, bool blowUpWalls);
	void printPath();
	bool makePathToOptimize1(int **currPlace, int **nextPlace, int* blowWalls, std::vector<int*>* errorPlaces);
};

