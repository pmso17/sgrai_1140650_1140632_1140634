#pragma once
#include "GL/glut.h"
#ifndef Particle
#define Particle
typedef float fVector[3];

typedef struct
{
	fVector position;
	fVector color;
}particle;

#endif

