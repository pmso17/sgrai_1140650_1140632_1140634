#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include "SOIL.h"
#include "SGRAI_Bomberman.h"
#include "CustomPlayer.h"
#include "CustomSound.h"
#include "Bomb.h"
#include "HorizontalBomb.h"
#include "VerticalBomb.h"
#include "CrossBomb.h"
#include "BoxBomb.h"
#include "RandomBomb.h"
#include "AtomicBomb.h"
#include "Sound.h"

using namespace std;

/* VARIAVEIS GLOBAIS */

#define FPS 24
#define DEBUG 1
#define NOME_TEXTURA_MENU_BG "Data/bg.png"
#define NOME_TEXTURA_SLIDER_BG "Data/stone.png"
#define NOME_TEXTURA_BOTAO_DIFFICULTY "Data/difficulty.png"
#define NOME_TEXTURA_BOTAO_TEXTURES "Data/playerTextures.png"
#define NOME_TEXTURA_BOTAO_SOUNDS "Data/sounds.png"


namespace Custom {


	typedef struct teclas_t {
		GLboolean   w, s, enter;
	}Teclas;

	typedef struct {
		GLfloat    x, y, z;
	}Pos;

	typedef struct {
		GLboolean	timerFlag;
		Sound*		music;
		GLboolean   doubleBuffer;
		GLint       delayMovimento;
		GLboolean   debug;
		teclas_t	teclas;
		GLint		selectedValue;
		Bomb*		selectionBomb;
		GLfloat     ratioWidth, ratioHeight;
	}Estado;

	Estado estado;

	GLfloat texCoords[][2] = {
		{ 0.0f, 0.0f },  // Canto Inferior Esquerdo
		{ 1.0f, 0.0f },  // Canto Inferior Direito
		{ 1.0f, 1.0f },   // Canto Superior Direito
		{ 0.0f, 1.0f }  // Canto Superior Esquerdo
	};

	GLint texture[5];

	Pos angle;

	void selectOption(GLint selectValue) {
		estado.timerFlag = GL_FALSE;
		estado.music->StopAudio();
		switch (selectValue)
		{
		case 0:
			CustomPlayer::Init();
			break;
		case 1:
			CustomSound::Init();
			break;
		default:
			printf("Error: %d\n", selectValue);
			break;
		}
	}

	int LoadGLTextures()                                    // Load Bitmaps And Convert To Textures
	{
		/* load an image file directly as a new OpenGL texture */
		texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[1] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO_TEXTURES, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[2] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO_SOUNDS, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[3] = SOIL_load_OGL_texture(NOME_TEXTURA_SLIDER_BG, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		texture[4] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO_DIFFICULTY, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

		if (texture[0] == 0) {
			return false;
		}


		// Typical Texture Generation Using Data From The Bitmap
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[3]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[4]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return true;
	}

	/* CALLBACK PARA REDIMENSIONAR JANELA */
	void Reshape(int width, int height)
	{
		estado.ratioHeight = height / 700.0f;
		estado.ratioWidth = width / 700.0f;
		glPushMatrix();
		glViewport(0, 0, (GLint)width, (GLint)height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-5, 5, -5, 5, -10, 10);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPopMatrix();
	}

	void desenhaBackGround()
	{
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glBegin(GL_POLYGON);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(-5, -5, 0);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(5, -5, 0);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(5, 5, 0);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(-5, 5, 0);
		glEnd();
	}

	void desenhaButton(GLfloat minX, GLfloat maxX, GLfloat minY, GLfloat maxY, GLint texNumber) {
		glBindTexture(GL_TEXTURE_2D, texture[texNumber]);
		glBegin(GL_POLYGON);
		//glColor3f(1,1,1);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(minX, minY, 0.1);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(maxX, minY, 0.1);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(maxX, maxY, 0.1);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(minX, maxY, 0.1);
		glEnd();
	}

	/* Callback de desenho */
	void Draw(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		/* ... chamada das rotinas auxiliares de desenho ... */
		glPushMatrix();
		desenhaBackGround();

		desenhaButton(-3, 3, 0.5, 2, 1); //Textures
		desenhaButton(-3, 3, -1.5, 0, 2);//Sound

		desenhaButton(-4, 4, -3.5, -2, 3);//Difficulty

		glPushMatrix();
		glTranslatef(0, 0, 20);
		glScalef(.3, .3, .3);
		glTranslated(36, 2.5 - (7 * estado.selectedValue), 0);
		glRotated(90, 0, 1, 0);
		estado.selectionBomb->Draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 20);
		glScalef(.3, .3, .3);
		glTranslated(60, 2.5 - (7 * estado.selectedValue), 0);
		glRotated(90, 0, 1, 0);
		estado.selectionBomb->Draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 0.1);
		glScalef(1, 0.5, 1);
		desenhaButton(-3.5 + ((Configurations::getDifficulty()-40)*0.029), -2 + ((Configurations::getDifficulty() - 40)*0.029), -6.3, -4.8, 4);//Music
		glPopMatrix();

		glPopMatrix();

		if (estado.doubleBuffer)
			glutSwapBuffers();
		else
			glFlush();

	}

	/*******************************
	***   callbacks timer   ***
	*******************************/
	/* Callback de temporizador */
	void Timer(int value)
	{
		clock_t drawStartTime = clock();
		glutPostRedisplay();
		clock_t drawEndTime = clock();

		int delayToNextFrame = (CLOCKS_PER_SEC / FPS) - (drawEndTime - drawStartTime);
		delayToNextFrame = floor(delayToNextFrame + 0.5);
		delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;

		if (estado.timerFlag)
		{
			glutTimerFunc(delayToNextFrame, Timer, 0);
		}
		/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
		somente os valores das vari�veis ... */

		/* redesenhar o ecr� */
		glutPostRedisplay();
	}

	/* Callback para interaccao via teclado (carregar na tecla) */
	void Key(unsigned char key, int x, int y)
	{
		switch (key) {
		case 27:
			estado.music->StopAudio();
			estado.timerFlag = GL_FALSE;
			MainInit();
			/* ... accoes sobre outras teclas ... */
			break;
		}

		if (DEBUG) {
			printf("Carregou na tecla %c\n", key);
		}
	}

	/* Callback para interaccao via teclado (largar a tecla) */
	void KeyUp(unsigned char key, int x, int y)
	{

		switch (key)
		{
		case 'w':
		case 'W':
			if (estado.selectedValue > 0)
			{
				estado.selectedValue--;
			}
			break;
		case's':
		case'S':
			if (estado.selectedValue < 1)
			{
				estado.selectedValue++;
			}
			break;
		case 13:
		case ' ':
			selectOption(estado.selectedValue);
			break;
		}

		if (DEBUG)
			printf("Largou a tecla %d\n", key);
	}

	void Mouse(int button, int state, int x, int y)
	{
		/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
		state  => GLUT_UP, GLUT_DOWN
		x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
		*/

		if (state == GLUT_UP)
		{
			switch (button) {
			case GLUT_LEFT_BUTTON:
				if (x >= 140 * estado.ratioWidth && x <= 560 * estado.ratioWidth && y >= 210 * estado.ratioHeight && y <= 315 * estado.ratioHeight) //1vs1
				{
					selectOption(0);
				}

				if (x >= 140 * estado.ratioWidth && x <= 560 * estado.ratioWidth && y >= 350 * estado.ratioHeight && y <= 455 * estado.ratioHeight) //1vspc
				{
					selectOption(1);
				}
				if (x >= 120 * estado.ratioWidth && x <= 575 * estado.ratioWidth && y >= 520 * estado.ratioHeight && y <= 570 * estado.ratioHeight) //Sound
				{
					int diff = ((float)(x - 120) * 200 / (575 - 120)) + 40;
					Configurations::setDifficulty(diff);
					printf("Hello %d\n", Configurations::getDifficulty());
				}
				break;
			case GLUT_MIDDLE_BUTTON:

				break;
			case GLUT_RIGHT_BUTTON:

				break;
			}
		}
		if (DEBUG)
			printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
	}

	void MousePassiveMotion(int x, int y)
	{
		//printf("%f e %f\n", estado.ratioHeight, estado.ratioWidth);
		if (x >= 140 * estado.ratioWidth && x <= 560 * estado.ratioWidth && y >= 210 * estado.ratioHeight && y <= 315 * estado.ratioHeight) //1vs1
		{
			estado.selectedValue = 0;
		}

		if (x >= 140 * estado.ratioWidth && x <= 560 * estado.ratioWidth && y >= 350 * estado.ratioHeight && y <= 455 * estado.ratioHeight) //1vspc
		{
			estado.selectedValue = 1;
		}
	}

	void selectMenuBomb() {
		int x = rand() % 7;
		switch (x) {
		case 0:
			estado.selectionBomb = new HorizontalBomb(0, 0);
			break;
		case 1:
			estado.selectionBomb = new RandomBomb(0, 0);
			break;
		case 2:
			estado.selectionBomb = new CrossBomb(0, 0);
			break;
		case 3:
			estado.selectionBomb = new VerticalBomb(0, 0);
			break;
		case 4:
			estado.selectionBomb = new BoxBomb(0, 0);
			break;
		case 5:
			estado.selectionBomb = new AtomicBomb(0, 0);
			break;
		default:
			estado.selectionBomb = new Bomb(0, 0);
			break;
		}
		estado.selectionBomb->setMenuBomb();
	}

	void Init() {
		estado.music = new  Sound("Data/Music/custom.wav",0);
		estado.music->InitAudio(true);
		estado.timerFlag = GL_TRUE;
		estado.selectedValue = 0;
		selectMenuBomb();
		estado.ratioHeight = 1;
		estado.ratioWidth = 1;
		estado.doubleBuffer = 1;
		estado.delayMovimento = 50;
		//glDisable(GL_TEXTURE_2D);
		/* Registar callbacks do GLUT */
		LoadGLTextures();
		/* callbacks de janelas/desenho */
		glutReshapeFunc(Reshape);
		glutDisplayFunc(Draw);

		glutKeyboardFunc(Key);
		glutKeyboardUpFunc(KeyUp);
		/* callbacks rato */
		glutPassiveMotionFunc(MousePassiveMotion);
		//glutMotionFunc(MouseMotion);
		glutMouseFunc(Mouse);

		/* callbacks timer/idle */
		glutTimerFunc(estado.delayMovimento, Timer, 0);
		//glutIdleFunc(Idle);
	}
}

