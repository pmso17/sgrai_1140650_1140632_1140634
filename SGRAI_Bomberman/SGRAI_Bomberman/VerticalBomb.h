#pragma once
#include "Bomb.h"
class VerticalBomb:public Bomb
{
public:
	VerticalBomb(GLint xPos, GLint zPos);
	~VerticalBomb();
	std::vector<GLint*> Explode() override;
};

