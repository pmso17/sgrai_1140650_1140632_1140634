#include "stdafx.h"
#include "add.h"

add::add() :operation()
{
}

add::~add()
{
}

void add::operate(int* add, int* toThis) {
	(*add)--;
	(*toThis)++;
}

void add::reverse(int* add, int* toThis) {
	(*add)++;
	(*toThis)--;
}
