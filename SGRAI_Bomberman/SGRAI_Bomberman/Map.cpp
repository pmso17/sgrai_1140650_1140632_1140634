#include "stdafx.h"
#include "Map.h"

Map::Map(GLint* texture, std::vector<Player*> players, Sound* music, GLint mode)
{
	deathMode = mode;
	listPlayers = players;
	musica = music;
	explosao = new Sound("Data/Music/explosion.wav", 1);
	textureForPlayer = texture;
	GLint razao = TAMANHO_MAPA / Block::GetTamanhoBlock();

	clearMatrix(razao);

	createStoneBlocks(razao);
	createWoodBlocks(razao);

	LoadGLTextures();
	CreateDisplayList();
}


Map::~Map()
{
}

void Map::clearMatrix(GLint razao) {
	for (int i = 0; i < 25; i++)
	{
		for (int j = 0; j < 25; j++)
		{
			mapa[i][j] = 0;
			mapaDanger[i][j] = 0;
		}
	}
}

void Map::createBlocksBox(GLint razao) {
	for (int i = 0; i < razao; i++)
	{
		listBlocks.insert(listBlocks.end(), new Block(i - (razao - 1) / 2, 12, false)); //Linha Norte 0-24
		mapa[i][0] = 1;
		listBlocks.insert(listBlocks.end(), new Block(i - (razao - 1) / 2, -12, false)); //Linha Sul 25-49
		mapa[i][24] = 1;
	}

	for (int j = 0; j < razao - 1; j++)
	{
		listBlocks.insert(listBlocks.end(), new Block(12, (j + 1) - (razao - 1) / 2, false)); //Linha Oeste 50-73
		mapa[0][j + 1] = 1;
		listBlocks.insert(listBlocks.end(), new Block(-12, (j + 1) - (razao - 1) / 2, false)); //Linha Este 74-98
		mapa[24][j + 1] = 1;
	}
}

void Map::createBlocksCentral(GLint razao) {
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 11; j++)
		{
			listBlocks.insert(listBlocks.end(), new Block((2 * (i + 1)) - (razao - 1) / 2, (2 * (j + 1)) - (razao - 1) / 2, false));
			mapa[2 * (i + 1)][2 * (j + 1)] = 1;
		}
	}
}

void Map::createStoneBlocks(GLint razao) {
	createBlocksBox(razao);

	createBlocksCentral(razao);
}

void Map::createWoodBlocks(GLint razao) {
	srand(static_cast <unsigned int> (time(NULL)));
	for (int i = 0; i < 50; i++)
	{
		GLint randomX = rand() % 24 + 1;
		GLint randomZ = rand() % 24 + 1;

		if (mapa[randomX][randomZ] == 0 && randomX != 12 && randomZ != 11 && randomX != 24 && randomZ != 1)// 11,12 � a posicao do player e 24,1 � a posicao do bot
		{
			listBlocks.insert(listBlocks.end(), new Block(randomX - (razao - 1) / 2, randomZ - (razao - 1) / 2, true));
			mapa[randomX][randomZ] = 2;
		}
		else {
			i--;//Ir� continuar no mesmo elemento
		}
	}
}

void Map::DrawMap() {
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glBegin(GL_POLYGON);
	glNormal3f(0, 1, 0);
	glTexCoord2fv(texCoords[0]);
	glVertex3f(-(TAMANHO_MAPA / 2), 0, -(TAMANHO_MAPA / 2));
	glTexCoord2fv(texCoords[1]);
	glVertex3f((TAMANHO_MAPA / 2), 0, -(TAMANHO_MAPA / 2));
	glTexCoord2fv(texCoords[2]);
	glVertex3f((TAMANHO_MAPA / 2), 0, (TAMANHO_MAPA / 2));
	glTexCoord2fv(texCoords[3]);
	glVertex3f(-(TAMANHO_MAPA / 2), 0, (TAMANHO_MAPA / 2));
	glEnd();
}

void Map::DrawBlocks() {

	GLint tex = SOIL_load_OGL_texture("Data/metal_block.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	for (int i = 0; i < 219; i++)
	{
		listBlocks[i]->Draw(tex);
	}
}

void Map::CreateDisplayList() {
	displayListMapa = glGenLists(1);
	glNewList(displayListMapa, GL_COMPILE);
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	DrawMap();
	DrawBlocks();
	glPopAttrib();
	glEndList();
}

void Map::Draw()
{
	GLint tex = SOIL_load_OGL_texture("Data/wood_block.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	for (unsigned int i = 219; i < listBlocks.size(); i++)
	{
		listBlocks[i]->Draw(tex);
	}

	for (unsigned int i = 0; i < listBombas.size(); i++)
	{
		listBombas[i]->Draw();
	}

	for (unsigned int i = 0; i < listExplosions.size(); i++)
	{
		if (listExplosions[i]->Draw())
		{
			listExplosions.erase(listExplosions.begin() + i);
		}
	}

	for (unsigned int i = 0; i < listPowerUps.size(); i++)
	{
		glPushMatrix();
		listPowerUps[i]->Draw();
		glPopMatrix();
	}

	glCallList(displayListMapa);
}

void Map::checkForExplosions(Player* player1, Player* player2) {
	for (unsigned i = 0; i < listBombas.size(); i++)
	{
		if (listBombas[i]->checkExplode()) {
			//printf("TYPE = %d\n", listBombas[i]->bombType); // OK

			explosao->InitAudio(false);
			GLint* posBomba = listBombas[i]->getPosition();
			GLint xBomba = posBomba[0] + (TAMANHO_MAPA) / 2;
			GLint zBomba = posBomba[1] + (TAMANHO_MAPA) / 2;
			xBomba = xBomba / 4;
			zBomba = zBomba / 4;
			mapa[xBomba][zBomba] = 0;

			std::vector<GLint*> blocosAfetados = listBombas[i]->Explode();

			//if ai
			for (unsigned i = 0; i < blocosAfetados.size(); i++)
			{
				int* place = blocosAfetados.at(i);
				mapaDanger[place[0]][place[1]]--;
			}
			//if ai

			createExplosions(blocosAfetados);

			checkDeaths(blocosAfetados, player1, player2);
			checkExploded(blocosAfetados);
			listBombas.erase(listBombas.begin() + i);
		}
	}
}

void Map::checkForPowerUps(Player* player1, Player* player2) {
	float * posP1 = player1->getPosicao();
	float * posP2 = player2->getPosicao();
	for (unsigned i = 0; i < listPowerUps.size(); i++)
	{
		GLint * posBomb = listPowerUps[i]->getPosition();

		if ((posP1[0] < posBomb[0] && posP1[0] + 2 > posBomb[0] || posP1[0] - 2 < posBomb[0] && posP1[0] > posBomb[0])
			&& (posP1[2] < posBomb[1] && posP1[2] + 2 > posBomb[1] || posP1[2] - 2 < posBomb[1] && posP1[2] > posBomb[1]))
		{
			player1->bombType = listPowerUps[i]->bombType;
			listPowerUps.erase(listPowerUps.begin() + i);
		}
		if ((posP2[0] < posBomb[0] && posP2[0] + 2 > posBomb[0] || posP2[0] - 2 < posBomb[0] && posP2[0] > posBomb[0])
			&& (posP2[2] < posBomb[1] && posP2[2] + 2 > posBomb[1] || posP2[2] - 2 < posBomb[1] && posP2[2] > posBomb[1]))
		{
			player2->bombType = listPowerUps[i]->bombType;
			listPowerUps.erase(listPowerUps.begin() + i);
		}
	}
}


void Map::createExplosions(std::vector<GLint*> blocos)
{
	for (unsigned int i = 0; i < blocos.size(); i++)
	{
		Explosion* newExplosion = new Explosion(blocos[i][0], blocos[i][1]);
		listExplosions.insert(listExplosions.end(), newExplosion);
	}
}

void Map::checkDeaths(std::vector<GLint*> blocosAfetados, Player* player1, Player* player2) {
	GLboolean player1Morto = false;
	GLboolean player2Morto = false;
	for (unsigned int i = 0; i < blocosAfetados.size(); i++)
	{
		GLfloat* player1Pos = player1->getPosicao();
		GLint xPlayer = player1Pos[0] + (TAMANHO_MAPA) / 2;
		GLint zPlayer = player1Pos[2] + (TAMANHO_MAPA) / 2;
		xPlayer = xPlayer / 4;
		zPlayer = zPlayer / 4;
		if (xPlayer == blocosAfetados[i][0] && zPlayer == blocosAfetados[i][1] && !player1Morto)
		{
			//printf("player1 morreu %d %d\n", xPlayer, zPlayer);
			player1Morto = true;
		}

		GLfloat* player2Pos = player2->getPosicao();
		xPlayer = player2Pos[0] + (TAMANHO_MAPA) / 2;
		zPlayer = player2Pos[2] + (TAMANHO_MAPA) / 2;
		xPlayer = xPlayer / 4;
		zPlayer = zPlayer / 4;
		if (xPlayer == blocosAfetados[i][0] && zPlayer == blocosAfetados[i][1] && !player2Morto)
		{
			//printf("player2 morreu  %d %d\n", xPlayer, zPlayer);
			player2Morto = true;

		}
	}
	if (player1Morto || player2Morto)
	{
		glDisable(GL_LIGHTING);
		Game1vsPC::timerKiller();
		Game1vs1::timerKiller();
		musica->StopAudio();
		explosao->StopAudio();
		if (player2Morto)
		{
			EndOfGame::Init(true, textureForPlayer, deathMode);
		}
		if (player1Morto)
		{
			EndOfGame::Init(false, textureForPlayer, deathMode);
		}
	}
}

void Map::checkExploded(std::vector<GLint*> blocos)
{
	for (unsigned int i = 0; i < blocos.size(); i++)
	{
		if (mapa[blocos[i][0]][blocos[i][1]] != 1 && mapa[blocos[i][0]][blocos[i][1]] != 3)
		{
			destroyBlock(blocos[i]);
			mapa[blocos[i][0]][blocos[i][1]] = 0;
		}
	}
}

void Map::spawnPowerUp(GLint* block) {
	if (rand() % 2)
	{
		int x = rand() % 6;
		Bomb * bomb = createSpecificBomb(x, block[0], block[1]);
		mapa[block[0]][block[1]] = 30;
		(*bomb).setAsPowerUp();
		listPowerUps.insert(listPowerUps.end(), bomb);
	}
}

void Map::destroyBlock(GLint * bloco)
{
	for (unsigned int i = 219; i < listBlocks.size(); i++)
	{
		GLint* posBlock = listBlocks[i]->getPosition();
		GLint xBlock = posBlock[0] + (TAMANHO_MAPA) / 2;
		GLint zBlock = posBlock[1] + (TAMANHO_MAPA) / 2;
		xBlock = xBlock / 4;
		zBlock = zBlock / 4;

		GLint cc[] = { xBlock, zBlock };
		if (bloco[0] == xBlock && bloco[1] == zBlock)
		{
			listBlocks.erase(listBlocks.begin() + i);
			spawnPowerUp(cc);
		}
	}
}


GLboolean Map::detetaColisoes(Player* player)
{
	GLboolean result = GL_FALSE;
	float* posPlayer = player->getPosicao();
	GLint xInferior = (posPlayer[0] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2;
	GLint zInferior = (posPlayer[2] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2;
	GLint xSuperior = (posPlayer[0] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2;
	GLint zSuperior = (posPlayer[2] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2;



	xInferior = xInferior / 4;
	zInferior = zInferior / 4;
	xSuperior = xSuperior / 4;
	zSuperior = zSuperior / 4;

	//printf("---------------------\n");
	//printf("XXXX%d e %d\n", xInferior, round(0.5 + (posPlayer[0] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4);
	//printf("ZZZZ%d e %d\n", zInferior, round(0.5 + (posPlayer[2] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4);
	//printf("XXXX%d e %d\n", xSuperior, round(0.5 + (posPlayer[0] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4);
	//printf("ZZZZ%d e %d\n", zSuperior, round(0.5 + (posPlayer[2] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4);
	//printf("---------------------\n");

	//printf("%d e %d\n",x,z);

	int* bombaPos = player->getBombaColocada();

	//printf("%d e %d\n", xInferior, bombaPos[0]);
	if (mapa[xInferior][zInferior] != 0 || mapa[xSuperior][zSuperior] != 0 || mapa[xInferior][zSuperior] != 0 || mapa[xSuperior][zInferior] != 0)
	{
		//printf("QQ?????\n");
		//printf("%d\n", mapa[xInferior][zInferior]);
		//printf("%d\n", mapa[xInferior][zSuperior]);
		//printf("%d\n", mapa[xSuperior][zInferior]);
		result = GL_TRUE;
	}

	//}

	if (player->inBomb)
	{

		result = GL_FALSE;
		if ((xSuperior == bombaPos[0] || xInferior == bombaPos[0]) && (zSuperior == bombaPos[1] || zInferior == bombaPos[1]))
		{
			result = GL_FALSE;
			if (xSuperior != bombaPos[0])
			{
				result = mapa[xSuperior][zInferior] != 0 || mapa[xSuperior][zSuperior] != 0;
			}
			if (xInferior != bombaPos[0])
			{
				result = mapa[xInferior][zInferior] != 0 || mapa[xInferior][zSuperior] != 0;
			}
			if (zSuperior != bombaPos[1])
			{
				result = mapa[xInferior][zSuperior] != 0 || mapa[xSuperior][zSuperior] != 0;
			}
			if (zInferior != bombaPos[1])
			{
				result = mapa[xInferior][zInferior] != 0 || mapa[xSuperior][zInferior] != 0;
			}
			//printf("MEOW\n");
		}
		else {
			player->inBomb = false;
		}
	}

	return result;
}

void Map::SpawnBomba(Player* player)
{
	float* posPlayer = player->getPosicao();
	GLint x = (posPlayer[0]) + (TAMANHO_MAPA) / 2;
	GLint z = (posPlayer[2]) + (TAMANHO_MAPA) / 2;

	x = x / 4;
	z = z / 4;

	if (mapa[x][z] == 0)
	{
		if (player->bombType == 1)
		{
			bool flag = true;
			do
			{
				x = rand() % 24 + 1;
				z = rand() % 24 + 1;
			} while (mapa[x][z] != 0);

		}
		Bomb* newBomb = createSpecificBomb(player->bombType, x, z);
		listBombas.push_back(newBomb);
		mapa[x][z] = 3;

		//if ai
		std::vector<GLint*> danger = newBomb->Explode();
		for (unsigned i = 0; i < danger.size(); i++)
		{
			int* place = danger.at(i);
			mapaDanger[place[0]][place[1]]++;
		}
		//if ai

		for (unsigned i = 0; i < listPlayers.size(); i++) {
			Player* thisPlayer = listPlayers.at(i);

			printf("%d -- %d\n", thisPlayer, player);

			if (thisPlayer != player) {
				float* posThisPlayer = thisPlayer->getPosicao();

				GLint zSuperior = ((posThisPlayer[2] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
				GLint xInferior = ((posThisPlayer[0] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
				GLint zInferior = ((posThisPlayer[2] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
				GLint xSuperior = ((posThisPlayer[0] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;

				if ((xInferior == x || xSuperior == x) && (zInferior == z || zSuperior == z))
				{
					thisPlayer->setBombaColocada(x, z);
				}
			}
			else {
				player->setBombaColocada(x, z);
			}
		}
	}
}

Bomb * Map::createSpecificBomb(int type, int x, int z) {
	Bomb * bomb;
	switch (type) {
	case 0:
		bomb = new HorizontalBomb(x, z);
		break;
	case 1:
		bomb = new RandomBomb(x, z);
		break;
	case 2:
		bomb = new CrossBomb(x, z);
		break;
	case 3:
		bomb = new VerticalBomb(x, z);
		break;
	case 4:
		bomb = new BoxBomb(x, z);
		break;
	case 5:
		bomb = new AtomicBomb(x, z);
		break;
	default:
		bomb = new Bomb(x, z);
		break;
	}
	return bomb;
}

int Map::LoadGLTextures()                                    // Load Bitmaps And Convert To Textures
{
	/* load an image file directly as a new OpenGL texture */
	texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_CHAO, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

	if (texture[0] == 0) {
		return false;
	}

	// Typical Texture Generation Using Data From The Bitmap
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return true;
}

void Map::givePlayerLocation(Player* player, int* poz) {
	float* posPlayer = player->getPosicao();
	poz[0] = ((posPlayer[0]) + (TAMANHO_MAPA) / 2) / 4;
	poz[1] = ((posPlayer[2]) + (TAMANHO_MAPA) / 2) / 4;
}

int Map::validateBlock(int* block) {
	if (block[0] > 0 && block[0] < 25 && block[1] > 0 && block[1] < 24)
	{
		return mapa[block[0]][block[1]];
	}
	return 1;
}