#pragma once
#include <stdlib.h>
#include <string>
#include "GL/glut.h"
class Configurations
{
public:
	static void setVolumeSound(GLfloat volume);
	static GLfloat getVolumeSound();
	static void setVolumeMusic(GLfloat volume);
	static GLfloat getVolumeMusic();
	static void setScaleCamera(GLfloat scale);
	static GLfloat getScaleCamera();
	static void setTextureNumber1(GLint number);
	static GLint getTextureNumber1();
	static void setTextureNumber2(GLint number);
	static GLint getTextureNumber2();
	static char* getGameMusicPath();
	static void setGameMusicPath(char* path);
	static GLint getDifficulty();
	static void setDifficulty(GLint difficult);
private:
	static GLint   difficulty;
	static GLfloat volumeSound;
	static GLfloat volumeMusic;
	static GLfloat scaleCamera;

	static GLint textureNumber1;
	static GLint textureNumber2;

	static char* gameMusicPath;
	Configurations();
	~Configurations();
};

