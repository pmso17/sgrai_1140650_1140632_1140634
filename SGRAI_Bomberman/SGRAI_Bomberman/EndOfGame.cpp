#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include "SOIL.h"
#include "SGRAI_Bomberman.h"
#include "CustomController.h"
#include "ParticlesGenerator.h"
#include "Sound.h"

using namespace std;

/* VARIAVEIS GLOBAIS */

#define DEBUG 1
#define FPS 24
#define NOME_TEXTURA_MENU_BG_WIN "Data/bg_win.png"
#define NOME_TEXTURA_MENU_BG_WIN1 "Data/bg_win1.png"
#define NOME_TEXTURA_MENU_BG_WIN2 "Data/bg_win2.png"
#define NOME_TEXTURA_MENU_BG_LOSE "Data/bg_lose.png"
#define NOME_TEXTURA_BOTAO_OK "Data/ok.png"


namespace EndOfGame {

	typedef struct {
		GLboolean	timerFlag;
		Sound*		gameOverSound;
		GLboolean   doubleBuffer;
		GLint       delayMovimento;
		GLboolean   debug;
		GLfloat		ratioWidth, ratioHeight;
	}Estado;

	Estado estado;

	GLfloat texCoords[][2] = {
		{ 0.0f, 0.0f },  // Canto Inferior Esquerdo
		{ 1.0f, 0.0f },  // Canto Inferior Direito
		{ 1.0f, 1.0f },   // Canto Superior Direito
		{ 0.0f, 1.0f }  // Canto Superior Esquerdo
	};

	GLint deathMode;
	GLint texture[4];
	GLint* texturePlayer;
	GLboolean victoryB;
	Player* player;

	int LoadGLTextures(GLboolean victory)                                    // Load Bitmaps And Convert To Textures
	{
		/* load an image file directly as a new OpenGL texture */

		if (deathMode == 0)
		{
			if (victory)
			{
				texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG_WIN, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
			}
			else {
				texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG_LOSE, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
			}

		}
		else {
			if (victory)
			{
				texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG_WIN1, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
			}
			else {
				texture[0] = SOIL_load_OGL_texture(NOME_TEXTURA_MENU_BG_WIN2, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
			}
		}

		texture[1] = SOIL_load_OGL_texture(NOME_TEXTURA_BOTAO_OK, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

		if (texture[0] == 0) {
			return false;
		}


		// Typical Texture Generation Using Data From The Bitmap
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, texture[1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return true;
	}

	/* CALLBACK PARA REDIMENSIONAR JANELA */
	void Reshape(int width, int height)
	{
		estado.ratioHeight = height / 700.0f;
		estado.ratioWidth = width / 700.0f;
		glViewport(0, 0, (GLint)width, (GLint)height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-5, 5, -5 * (GLdouble)height / width, 5 * (GLdouble)height / width, -10, 10);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	void desenhaBackGround()
	{
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glBegin(GL_POLYGON);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(-5, -5, 0);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(5, -5, 0);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(5, 5, 0);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(-5, 5, 0);
		glEnd();
	}

	void desenhaButton(GLfloat minX, GLfloat maxX, GLfloat minY, GLfloat maxY, GLint texNumber) {
		glBindTexture(GL_TEXTURE_2D, texture[texNumber]);
		glBegin(GL_POLYGON);
		//glColor3f(1,1,1);
		glTexCoord2fv(texCoords[0]);
		glVertex3f(minX, minY, 0.1);
		glTexCoord2fv(texCoords[1]);
		glVertex3f(maxX, minY, 0.1);
		glTexCoord2fv(texCoords[2]);
		glVertex3f(maxX, maxY, 0.1);
		glTexCoord2fv(texCoords[3]);
		glVertex3f(minX, maxY, 0.1);
		glEnd();
	}

	/* Callback de desenho */
	void Draw(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		/* ... chamada das rotinas auxiliares de desenho ... */
		glPushMatrix();
		desenhaBackGround();
		desenhaButton(-2, 2, -4.5, -3.75, 1);
		if (victoryB || deathMode == 1)
		{
			player->VictoryDance();
			player->Draw(texturePlayer);
		}
		if (estado.doubleBuffer)
			glutSwapBuffers();
		else
			glFlush();

	}

	/*******************************
	***   callbacks timer   ***
	*******************************/
	/* Callback de temporizador */
	void Timer(int value)
	{
		clock_t drawStartTime = clock();
		glutPostRedisplay();
		clock_t drawEndTime = clock();

		int delayToNextFrame = (CLOCKS_PER_SEC / FPS) - (drawEndTime - drawStartTime);
		delayToNextFrame = floor(delayToNextFrame + 0.5);
		delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;

		if (estado.timerFlag)
		{
			glutTimerFunc(delayToNextFrame, Timer, 0);
		}
		/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
		somente os valores das vari�veis ... */

		/* redesenhar o ecr� */
		glutPostRedisplay();
	}

	/* Callback para interaccao via teclado (carregar na tecla) */
	void Key(unsigned char key, int x, int y)
	{
		switch (key) {
		case 27:
			estado.timerFlag = GL_FALSE;
			estado.gameOverSound->StopAudio();
			MainInit();
			/* ... accoes sobre outras teclas ... */
			break;
		}

		if (DEBUG) {
			printf("Carregou na tecla %c\n", key);
		}
	}

	/* Callback para interaccao via teclado (largar a tecla) */
	void KeyUp(unsigned char key, int x, int y)
	{
		switch (key)
		{
		case 13:
		case ' ':
			estado.timerFlag = GL_FALSE;
			estado.gameOverSound->StopAudio();
			MainInit();
			break;
		default:
			break;
		}
		if (DEBUG)
			printf("Largou a tecla %c\n", key);
	}

	/* Callback para interaccao via teclas especiais  (carregar na tecla) */
	void SpecialKey(int key, int x, int y)
	{
		/* ... accoes sobre outras teclas especiais ...
		GLUT_KEY_F1 ... GLUT_KEY_F12
		GLUT_KEY_UP
		GLUT_KEY_DOWN
		GLUT_KEY_LEFT
		GLUT_KEY_RIGHT
		GLUT_KEY_PAGE_UP
		GLUT_KEY_PAGE_DOWN
		GLUT_KEY_HOME
		GLUT_KEY_END
		GLUT_KEY_INSERT
		*/

		if (DEBUG)
			printf("Carregou na tecla especial %d\n", key);
	}

	/* Callback para interaccao via teclas especiais (largar na tecla) */
	void SpecialKeyUp(int key, int x, int y)
	{
		if (DEBUG)
			printf("Largou a tecla especial %d\n", key);
	}

	void Mouse(int button, int state, int x, int y)
	{
		/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
		state  => GLUT_UP, GLUT_DOWN
		x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
		*/

		if (state == GLUT_UP)
		{
			switch (button) {
			case GLUT_LEFT_BUTTON:
				if (x >= 210 * estado.ratioWidth && x <= 490 * estado.ratioWidth && y >= 610 * estado.ratioHeight && y <= 655 * estado.ratioHeight)//Ok
				{
					estado.timerFlag = GL_FALSE;
					estado.gameOverSound->StopAudio();
					MainInit();
				}
				break;
			case GLUT_MIDDLE_BUTTON:

				break;
			case GLUT_RIGHT_BUTTON:

				break;
			}
		}
		if (DEBUG)
			printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
	}

	void Init(GLboolean victory, GLint* textureForPlayer, GLint mode) {
		estado.ratioHeight = 1;
		estado.ratioWidth = 1;
		deathMode = mode;
		victoryB = victory;
		if (victoryB)
		{
			estado.gameOverSound = new  Sound("Data/Music/win.wav", 0);
			estado.gameOverSound->InitAudio(false);
		}
		else {
			estado.gameOverSound = new  Sound("Data/Music/gameover.wav", 0);
			estado.gameOverSound->InitAudio(false);
		}
		texturePlayer = textureForPlayer;
		player = new Player(false);
		player->PrepareVictoryDance();
		estado.timerFlag = GL_TRUE;
		estado.doubleBuffer = 1;
		estado.delayMovimento = 50;
		/* Registar callbacks do GLUT */
		LoadGLTextures(victory);
		/* callbacks de janelas/desenho */
		glutReshapeFunc(Reshape);
		glutDisplayFunc(Draw);

		/* Callbacks de teclado */
		glutKeyboardFunc(Key);
		glutKeyboardUpFunc(KeyUp);
		glutSpecialFunc(SpecialKey);
		glutSpecialUpFunc(SpecialKeyUp);

		/* callbacks rato */
		//glutPassiveMotionFunc(MousePassiveMotion);
		//glutMotionFunc(MouseMotion);
		glutMouseFunc(Mouse);

		/* callbacks timer/idle */
		glutTimerFunc(estado.delayMovimento, Timer, 0);
		//glutIdleFunc(Idle);
	}
}

