#include "stdafx.h"
#include "Configurations.h"

GLfloat		 Configurations::scaleCamera = 0.2;
GLfloat		 Configurations::volumeMusic = 1;
GLfloat		 Configurations::volumeSound = 1;

GLint		 Configurations::textureNumber1 = 0;
GLint		 Configurations::textureNumber2 = 0;

GLint		 Configurations::difficulty = 40;

char*  Configurations::gameMusicPath = "Data/Music/game.wav";

void Configurations::setVolumeSound(GLfloat volume)
{
	volumeSound = volume;
}

GLfloat Configurations::getVolumeSound()
{
	return volumeSound;
}

void Configurations::setVolumeMusic(GLfloat volume)
{
	volumeMusic = volume;
}

GLfloat Configurations::getVolumeMusic()
{
	return volumeMusic;
}

void Configurations::setScaleCamera(GLfloat scale)
{
	scaleCamera = scale;
}

GLfloat Configurations::getScaleCamera()
{
	return scaleCamera;
}

void Configurations::setTextureNumber1(GLint number)
{
	textureNumber1 = number;
}

GLint Configurations::getTextureNumber1()
{
	return textureNumber1;
}

void Configurations::setTextureNumber2(GLint number)
{
	textureNumber2 = number;
}

GLint Configurations::getTextureNumber2()
{
	return textureNumber2;
}

char* Configurations::getGameMusicPath()
{
	return gameMusicPath;
}

void Configurations::setGameMusicPath(char* path)
{
	gameMusicPath = path;
}

GLint Configurations::getDifficulty()
{
	return difficulty;
}

void Configurations::setDifficulty(GLint difficult)
{
	difficulty = difficult;
}

Configurations::Configurations()
{
}


Configurations::~Configurations()
{
}
