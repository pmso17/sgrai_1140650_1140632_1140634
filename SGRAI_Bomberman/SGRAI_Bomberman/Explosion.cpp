#include "stdafx.h"
#include "Explosion.h"


Explosion::Explosion(GLfloat posX,GLfloat posZ)
{
	x = (posX-12)*4;
	z = (posZ-12)*4;
	//printf("%f e %f\n",x,z);
	time = 10000000;
	particle = new ParticlesGenerator();
}


Explosion::~Explosion()
{
}

GLboolean Explosion::Draw()
{
	if (time > 0)
	{
		glPushMatrix();
		glTranslatef(x, 1, z);
		glScalef(4,4,4);
		particle->UpdateParticles();
		particle->Draw();
		time-=0.0000001;
		glPopMatrix();
		return GL_TRUE;
	}
	else {
		return GL_FALSE;
	}
}
