#pragma once
#include "GL/glut.h"
#include "PartBuilder.h"
#define TAMANHO_BLOCK 4
class Block
{
public:
	Block(GLint x, GLint z,GLboolean destructible);
	~Block();
	static GLint GetTamanhoBlock();
	void Draw(GLint texture);
	GLint * getPosition();
private:
	GLint x,y, z;
	PartBuilder* partBuilder;
	GLboolean isDestructible;
	GLint blockLife;
};

