#include "stdafx.h"
#include "Game1vsPCController.h"


Game1vsPCController::Game1vsPCController(Sound* music)
{
	configs = readConfigFile();
	LoadGLTextures(Configurations::getTextureNumber1());
	player = new Player(true);
	computer = new Player(true, 8, -4);
	std::vector<Player*> list;
	list.insert(list.end(), player);
	list.insert(list.end(), computer);
	mapa = new Map(texture, list, music, 0);

	camara = new Camara();

	int a[2];
	mapa->givePlayerLocation(computer, a);
	path.insert(path.begin(), new int[2]{ a[0],a[1] });
	ac = 0;
	tics = 0;
}


Game1vsPCController::~Game1vsPCController()
{
}

GLint* Game1vsPCController::getTexture() {
	return texture;
}

void Game1vsPCController::Draw(GLfloat walking, GLfloat rotacaoPlayer, GLfloat rotacaoCamaraHorizontal, GLfloat rotacaoCamaraVertical, GLfloat raioCamara)
{
	float* posPlayer = player->getPosicao();
	camara->setCamera(rotacaoCamaraHorizontal, rotacaoCamaraVertical, raioCamara, posPlayer);

	mapa->checkForExplosions(player, computer);
	mapa->checkForPowerUps(player, computer);
	mapa->Draw();

	if (walking != 0)
	{
		player->Andar();
	}

	player->Move(walking, rotacaoPlayer);
	if (mapa->detetaColisoes(player))
	{
		player->Move(-walking, rotacaoPlayer);
	}
	computerThink();

	computer->Draw(texture);
	player->Draw(texture);

}

/*
1� testar se existe uma bomba por perto
2� testar se o player est� por perto
3� colocar bombas
*/
void Game1vsPCController::computerThink()
{
	switch (ac)
	{
	case 2://wait for tics
		if (checkIfInDanger(path.front()))
		{
			aiThinking1();
			return;
		}
		if (tics > 0) {
			tics--;
		}
		else {
			ac = 0;
		}
		return;
	case 10://give up
		return;
	default:
		break;
	}
	/*
	first poz of path is current poz.
	*/

	//check if at end of path
	if (path.size() == 1) {//at the end of path
		aiThinking1();
	}
	else {
		GLfloat* player2Pos = computer->getPosicao();

		GLint rotacao = player2Pos[3];

		GLint xInferior = ((player2Pos[0] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
		GLint zInferior = ((player2Pos[2] - COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
		GLint xSuperior = ((player2Pos[0] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;
		GLint zSuperior = ((player2Pos[2] + COLISION_BOX_DIMENSION) + (TAMANHO_MAPA) / 2) / 4;

		if (xInferior == xSuperior && zInferior == zSuperior) {//check if its completely inside a block
			if (xInferior == path.at(1)[0] && zInferior == path.at(1)[1]) {//on next block, update list,rotate to next block
				path.erase(path.begin());
				aiThinking1();
				return;
			}
			int* start = path.front();
			int* next = path.at(1);

			if (start[0] == next[0]) {//move on z
				if (start[1] > next[1]) {//z-
					rotacao = -180;
				}
				else {//z+
					rotacao = 0;
				}
			}
			else {//move on x

				if (start[0] > next[0]) {//x-
					rotacao = -90;
				}
				else {//x+
					rotacao = 90;
				}
			}
		}
		computer->Andar();
		computer->Move(PC_SPEED, rotacao);
		if (mapa->detetaColisoes(computer))
		{
			computer->Move(-PC_SPEED, rotacao);
			aiThinking1();
		}
	}
}

//ai
void Game1vsPCController::aiThinking1() {
	//check for danger
	bool inDanger = checkIfInDanger(path.front());
	if (inDanger) {
		if (ac != 1) {//not running from bombs
			ac = 1;//running from bombs
			runFromBombs();
			return;
		}
		else {//check if last pozitions is still safe
			if (checkIfInDanger(path.back())) {
				runFromBombs();
				return;
			}
		}
	}

	//next step sdsadawdsxgd
	switch (ac)
	{
	case 0://doing nothing
		int playerPoz[2], pcPoz[2];
		mapa->givePlayerLocation(player, playerPoz);
		mapa->givePlayerLocation(computer, pcPoz);

		if (playerPoz[0] == pcPoz[0] && playerPoz[1] == pcPoz[1]) {
			mapa->SpawnBomba(computer);
			aiThinking1();
		}
		else {
			ac = 3;//moving to player
			makePathTo(playerPoz, true/*blow up walls*/);

			//check if next block is a wood block
			if (path.size() > 1)
			{
				if (mapa->validateBlock(path.at(1)) == 2) {
					mapa->SpawnBomba(computer);
					aiThinking1();
				}
			}
		}
		return;
	case 1://running from bomb
		if (!inDanger) {
			//prob needs to be more complex
			ac = 2;//wait for 24*4 tics?????
			tics = Configurations::getDifficulty();;
		}
		return;
	case 3://moving to player
		if (path.size() > 1) {//on the way to player

			//check if next block is a wood block
			if (mapa->validateBlock(path.at(1)) == 2) {
				mapa->SpawnBomba(computer);
				aiThinking1();
				return;
			}

			//check if player moved
			int playerPoz[2];
			mapa->givePlayerLocation(player, playerPoz);
			if (playerPoz[0] != path.back()[0] || playerPoz[1] != path.back()[1]) {
				makePathTo(playerPoz, true/*blow up walls*/);
			}
		}
		else {//reached end of path
			ac = 0;
		}
		return;
	default:
		if (path.size() == 1) {
			ac = 0;//doing nothing
		}
		break;
	}
}

void Game1vsPCController::runFromBombs() {
	int* start = path.front();
	std::vector<int*> processedPlaces;
	std::vector<int*> auxPlaces;
	auxPlaces.insert(auxPlaces.end(), start);
	unsigned i = 0, size = auxPlaces.size();
	int tolerance = 1, aux;

	//find closeste safe place
	do {
		while (i < size && checkIfInDanger(auxPlaces.at(i))) {
			int* currPlace = auxPlaces.at(i);
			processedPlaces.insert(processedPlaces.end(), currPlace);

			int* validate = new int[2]{ currPlace[0] - 1, currPlace[1] };
			aux = mapa->validateBlock(validate);
			if ((aux == 0 || aux == 30) && !checkRepeated(&auxPlaces, validate))
			{
				auxPlaces.insert(auxPlaces.end(), validate);
			}

			validate = new int[2]{ currPlace[0], currPlace[1] - 1 };
			aux = mapa->validateBlock(validate);
			if ((aux == 0 || aux == 30) && !checkRepeated(&auxPlaces, validate))
			{
				auxPlaces.insert(auxPlaces.end(), validate);
			}

			validate = new int[2]{ currPlace[0] + 1, currPlace[1] };
			aux = mapa->validateBlock(validate);
			if ((aux == 0 || aux == 30) && !checkRepeated(&auxPlaces, validate))
			{
				auxPlaces.insert(auxPlaces.end(), validate);
			}

			validate = new int[2]{ currPlace[0], currPlace[1] + 1 };
			aux = mapa->validateBlock(validate);
			if ((aux == 0 || aux == 30) && !checkRepeated(&auxPlaces, validate))
			{
				auxPlaces.insert(auxPlaces.end(), validate);
			}

			i++;
			size = auxPlaces.size();
		}


		if (i == auxPlaces.size()) {// = rip no place to hide
			path.clear();
			path.insert(path.end(), auxPlaces.at(0));
			ac = 10;//give up
			return;
		}

		//create path to sead safe place
		makePathTo(auxPlaces.at(i), false/*avoid walls*/);

		tolerance = (i % 3) + 1;
		i++;
	} while (false/*checkPath(tolerance)*/);
}

bool Game1vsPCController::makePathToOptimize1(int **currPlace, int **nextPlace, int* blowWalls, std::vector<int*>* errorPlaces) {
	int aux = mapa->validateBlock(*nextPlace);
	if ((aux == 0 || aux == 30 || aux == *blowWalls) && !checkRepeated(errorPlaces, *nextPlace)) {
		path.insert(path.end(), *nextPlace);
		errorPlaces->insert(errorPlaces->end(), *nextPlace);
		*currPlace = *nextPlace;
		*nextPlace = new int[2]{ (*currPlace)[0],(*currPlace)[1] };
		return true;
	}
	return false;
}

void Game1vsPCController::printPath() {
	printf("path:\n");
	for (unsigned i = 0; i < path.size(); i++)
	{
		printf("		X%d:--Z%d:\n", path.at(i)[0], path.at(i)[1]);
	}
	printf("path------------\n");
}

void Game1vsPCController::makePathTo(int* end, bool blowUpWalls) {
	int blowWalls = 0;
	if (blowUpWalls)
	{
		blowWalls = 2;
	}
	//start
	int* currPlace = path.front();
	int disX = end[0] - currPlace[0], disZ = end[1] - currPlace[1];

	operation *opX, *opZ;
	/*
	printf("CREATING PATH------------------\n");
	printf("X%d--Z%d--dis:", currPlace[0], currPlace[1]);
	printf("X%d--Z%d--obj:", disX, disZ);
	printf("X%d--Z%d\n", end[0], end[1]);
	*/
	path.clear();
	std::vector<int*> errorPlaces;
	int *nextPlace = new int[2]{ currPlace[0],currPlace[1] };
	bool fuckedUP = false;

	path.insert(path.end(), currPlace);
	while ((disX != 0 || disZ != 0) && !fuckedUP) {
		if (disX > 0)
		{
			//printf("opX:+\n");
			opX = new add();
		}
		else {
			//printf("opX:-\n");
			opX = new sub();
		}
		if (disZ > 0)
		{
			//printf("opZ:+\n");
			opZ = new add();
		}
		else {
			//printf("opZ:-\n");
			opZ = new sub();
		}


		while (disX != 0)
		{
			//printf("disX:%d\n", disX);
			opX->operate(&disX, &nextPlace[0]);

			if (makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
			{
				//printPath();
			}
			else {
				opX->reverse(&disX, &nextPlace[0]);
				opZ->operate(&disZ, &nextPlace[1]);
				if (makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
				{
					//printPath();
				}
				else {
					opZ->reverse(&disZ, &nextPlace[1]);
					opX->reverse(&disX, &nextPlace[0]);
					if (path.size() > 1)
					{
						path.pop_back();
						currPlace = path.back();
						//printPath();
					}
					else {
						//printPath();
						nextPlace = new int[2]{ path.front()[0],path.front()[1] };
						opX->reverse(&disX, &nextPlace[0]);
						if (!makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
						{
							opX->operate(&disX, &nextPlace[0]);
							opZ->reverse(&disZ, &nextPlace[1]);
							makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces);
						}
						//printPath();
						fuckedUP = true;
					}
					//printf("RIP1:X%d--Z%d---", currPlace[0], currPlace[1]);
					//printf("X%d--Z%d\n", nextPlace[0], nextPlace[1]);
					//QQ rip ? !"#!" ?
					break;
				}
			}
		}

		if (fuckedUP)
		{
			break;
		}

		if (disX > 0)
		{
			//printf("opX:+\n");
			opX = new add();
		}
		else {
			//printf("opX:-\n");
			opX = new sub();
		}
		if (disZ > 0)
		{
			//printf("opZ:+\n");
			opZ = new add();
		}
		else {
			//printf("opZ:-\n");
			opZ = new sub();
		}

		while (disZ != 0) {
			//printf("disZ:%d\n", disZ);
			opZ->operate(&disZ, &nextPlace[1]);
			if (makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
			{
				////printPath();
			}
			else {
				opZ->reverse(&disZ, &nextPlace[1]);
				opX->operate(&disX, &nextPlace[0]);
				if (makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
				{
					////printPath();
				}
				else {
					opZ->reverse(&disZ, &nextPlace[1]);
					opX->reverse(&disX, &nextPlace[0]);
					if (path.size() > 1)
					{
						path.pop_back();
						currPlace = path.back();
						////printPath();
					}
					else {
						////printPath();
						nextPlace = new int[2]{ path.front()[0],path.front()[1] };
						opZ->reverse(&disZ, &nextPlace[1]);
						if (!makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces))
						{
							opZ->operate(&disZ, &nextPlace[1]);
							opX->reverse(&disX, &nextPlace[0]);
							makePathToOptimize1(&currPlace, &nextPlace, &blowWalls, &errorPlaces);
						}
						////printPath();
						fuckedUP = true;
					}
					/*
					printf("RIP2:X%d--Z%d---", currPlace[0], currPlace[1]);
					printf("X%d--Z%d\n", nextPlace[0], nextPlace[1]);
					*/
					//QQ rip ? !"#!" ?
					break;
				}

			}
		}
	}
	//printf("CREATED------------\n\n");
}

bool Game1vsPCController::checkPath(int i) {
	for (unsigned j = 0; j < path.size(); j++) {
		if (checkIfInDanger(path.at(j)) > i)
		{
			printf("not safest path");
			return true;
		}
	}
	return false;
}

char Game1vsPCController::checkIfInDanger(int* pcLocation) {
	return mapa->mapaDanger[pcLocation[0]][pcLocation[1]];
}

bool Game1vsPCController::checkRepeated(std::vector<int*>* auxPlaces, int* validate) {
	for (unsigned i = 0; i < auxPlaces->size(); i++) {
		int* vec = auxPlaces->at(i);
		if (vec[0] == validate[0] && vec[1] == validate[1]) {
			return true;
		}
	}
	return false;
}

void Game1vsPCController::LoadGLTexturesHead(GLint position) {

	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_front.png");
	texture[0] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_back.png");
	texture[1] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_lateral.png");
	texture[2] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_up.png");
	texture[3] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/head_down.png");
	texture[4] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vsPCController::LoadGLTexturesBody(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_front.png");
	texture[5] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_back.png");
	texture[6] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_lateral.png");
	texture[7] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_up.png");
	texture[8] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/body_down.png");
	texture[9] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vsPCController::LoadGLTexturesArm(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_front.png");
	texture[10] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_back.png");
	texture[11] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_lateral.png");
	texture[12] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_up.png");
	texture[13] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/arm_down.png");
	texture[14] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

void Game1vsPCController::LoadGLTexturesLeg(GLint position) {
	char* helperString = new char[100];

	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_front.png");
	texture[15] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_back.png");
	texture[16] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_lateral.png");
	texture[17] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_up.png");
	texture[18] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	strcpy_s(helperString, 50, configs[position].pastaSkins);
	strcat_s(helperString, 100, "/leg_down.png");
	texture[19] = SOIL_load_OGL_texture(helperString, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}

int Game1vsPCController::LoadGLTextures(GLint position)                                    // Load Bitmaps And Convert To Textures
{
	/* load an image file directly as a new OpenGL texture */
	LoadGLTexturesHead(position);
	LoadGLTexturesBody(position);
	LoadGLTexturesArm(position);
	LoadGLTexturesLeg(position);

	for (int i = 0; i < 20; i++)
	{
		if (texture[i] == 0) {
			return false;
		}
	}


	// Typical Texture Generation Using Data From The Bitmap
	for (int i = 0; i < 20; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texture[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	return true;
}


void Game1vsPCController::ColocarBomba() {
	float* posPlayer = player->getPosicao();
	mapa->SpawnBomba(player);
}

void Game1vsPCController::ChangeCamera() {
	camara->changeCamera();
}