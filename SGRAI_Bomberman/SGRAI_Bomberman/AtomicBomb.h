#pragma once
#include "Bomb.h"
class AtomicBomb : public Bomb
{
public:
	AtomicBomb(GLint xPos, GLint zPos);
	~AtomicBomb();
	void Draw() override;
	std::vector<GLint*> Explode() override;
};

