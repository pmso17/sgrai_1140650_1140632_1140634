#pragma once
#include <stdlib.h>
#include <vector>
#include "SOIL.h"
#include "ParticlesGenerator.h"
#include "GL/glut.h"
#include "PartBuilder.h"
#define TAMANHO_MAPA 100
class Bomb
{
public:
	Bomb(GLint xPos, GLint zPos);
	~Bomb();
	virtual void Draw();
	GLint* getPosition();
	bool checkExplode();
	void setAsPowerUp();
	int bombType;
	virtual std::vector<GLint*> Explode();
	void setMenuBomb();
protected:
	GLint x;
	GLint z;
	GLfloat time=1;//seconds
	GLint texture[1];
	bool isBomb = true;
	bool menuBomb = false;
private:
	ParticlesGenerator* particleGenerator;
};

