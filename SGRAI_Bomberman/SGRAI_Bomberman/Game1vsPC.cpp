#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <GL/glut.h>
#include "SOIL.h"
#include "SGRAI_Bomberman.h"
#include "Game1vsPCController.h"
#include "Sound.h"
#include "Configurations.h"

using namespace std;

/* VARIAVEIS GLOBAIS */

#define DEBUG 0
#define ANDAR_VELOCITY .4 //Usar 0.1 
#define FPS 24


namespace Game1vsPC {

	typedef struct {
		GLfloat    x, y, z;
	}Pos;

	typedef struct teclas_t {
		GLboolean   i, j, k, l,
			w, a, s, d,
			e, o,
			plus, minus;
	}Teclas;

	typedef struct {
		GLfloat rotacaoPlayer;
	}Modelo;

	typedef struct {
		GLboolean	timerFlag;
		Sound*		music;
		GLboolean   doubleBuffer;
		GLint       delayMovimento;
		GLboolean   debug;
		teclas_t    teclas;
		Modelo		player;
		GLfloat fovCamera, rotacaoCameraHorizontal, rotacaoCameraVertical, raioCamera;
	}Estado;



	Estado estado;

	GLfloat andar = 0;

	Game1vsPCController* controller;

	void setLight()
	{
		GLfloat light_pos[4] = { -3.0f, 20.0f, 1.8f, 1.1f };
		GLfloat light_ambient[] = { 0.4f, 0.4f, 0.4f, 2.0f };
		GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		GLfloat light_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };

		// ligar iluminacao
		glEnable(GL_LIGHTING);

		// ligar e definir fonte de luz 0
		glEnable(GL_LIGHT0);
		glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
		glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	}

	void setMaterial()
	{
		GLfloat mat_specular[] = { 0.6f, 0.6f, 0.6f, 1.0f };
		GLfloat mat_shininess = 104;

		// criacao automatica das componentes Ambiente e Difusa do material a partir das cores
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

		// definir de outros parametros dos materiais esteticamente
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
	}

	void Reshape(int width, int height)
	{
		//BLACKSCREEN BORDER
		GLfloat aspect = 1;

		int screenWidth = width;
		int screenHeight = height;

		int viewWidth = screenWidth;
		int viewHeight = screenWidth / aspect;

		if (viewHeight > screenHeight) {
			viewHeight = screenHeight;
			viewWidth = screenHeight * aspect;
		}

		int vportX = (screenWidth - viewWidth) / 2;
		int vportY = (screenHeight - viewHeight) / 2;

		glViewport(vportX, vportY, viewWidth, viewHeight);
	}

	/* Callback de desenho */
	void Draw(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glPushMatrix();

		glLoadIdentity();

		/* ... chamada das rotinas auxiliares de desenho ... */
		glPushMatrix();
		glColor3f(1, 1, 1);
		controller->Draw(andar, estado.player.rotacaoPlayer, estado.rotacaoCameraHorizontal, estado.rotacaoCameraVertical, estado.raioCamera);
		glPopMatrix();
		glPopMatrix();

		if (estado.doubleBuffer)
			glutSwapBuffers();
		else
			glFlush();

	}

	/*******************************
	***   callbacks timer   ***
	*******************************/
	/* Callback de temporizador */

	void timerKiller() {
		glDisable(GL_LIGHTING);
		estado.timerFlag = GL_FALSE;
	}

	void Timer(int value)
	{
		clock_t drawStartTime = clock();
		glutPostRedisplay();
		clock_t drawEndTime = clock();

		int delayToNextFrame = (CLOCKS_PER_SEC / FPS) - (drawEndTime - drawStartTime);
		delayToNextFrame = floor(delayToNextFrame + 0.5);
		delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;

		if (estado.timerFlag)
		{
			glutTimerFunc(delayToNextFrame, Timer, 0);
		}
		/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
		somente os valores das vari�veis ... */

		if (estado.teclas.a) {
			estado.player.rotacaoPlayer += 5;
		}
		if (estado.teclas.d) {
			estado.player.rotacaoPlayer -= 5;
		}

		if (estado.teclas.j)
		{
			estado.rotacaoCameraHorizontal += 5;
		}

		if (estado.teclas.l)
		{
			estado.rotacaoCameraHorizontal -= 5;
		}

		if (estado.teclas.i && estado.rotacaoCameraVertical < 90)
		{
			estado.rotacaoCameraVertical += 5;
		}

		if (estado.teclas.k && estado.rotacaoCameraVertical > 15)
		{
			estado.rotacaoCameraVertical -= 5;
		}


		/* redesenhar o ecr� */
		glutPostRedisplay();
	}

	/* Callback para interaccao via teclado (carregar na tecla) */
	void Key(unsigned char key, int x, int y)
	{
		switch (key) {
		case 27:
			glDisable(GL_LIGHTING);
			controller->~Game1vsPCController();
			estado.timerFlag = GL_FALSE;
			estado.music->StopAudio();
			MainInit();
			/* ... accoes sobre outras teclas ... */
			break;
		case'i':
		case'I':
			estado.teclas.i = GL_TRUE;
			break;
		case'j':
		case'J':
			estado.teclas.j = GL_TRUE;
			break;
		case'k':
		case'K':
			estado.teclas.k = GL_TRUE;
			break;
		case'l':
		case'L':
			estado.teclas.l = GL_TRUE;
			break;
		case'w':
		case'W':
			estado.teclas.w = GL_TRUE;
			andar = ANDAR_VELOCITY;
			break;
		case'a':
		case'A':
			estado.teclas.a = GL_TRUE;
			break;
		case's':
		case'S':
			estado.teclas.s = GL_TRUE;
			andar = -ANDAR_VELOCITY;
			break;
		case'd':
		case'D':
			estado.teclas.d = GL_TRUE;
			break;
		case '+':
			if (Configurations::getScaleCamera() < 1.0f)
			{
				Configurations::setScaleCamera(Configurations::getScaleCamera()+0.01);
			}
			break;
		case '-':
			if (Configurations::getScaleCamera() > 0.1f)
			{
				Configurations::setScaleCamera(Configurations::getScaleCamera() - 0.01);
			}
			break;
		}

		if (DEBUG) {
			printf("Carregou na tecla %c\n", key);
		}
	}

	/* Callback para interaccao via teclado (largar a tecla) */
	void KeyUp(unsigned char key, int x, int y)
	{

		switch (key)
		{
		case'i':
		case'I':
			estado.teclas.i = GL_FALSE;
			break;
		case'j':
		case'J':
			estado.teclas.j = GL_FALSE;
			break;
		case'k':
		case'K':
			estado.teclas.k = GL_FALSE;
			break;
		case'l':
		case'L':
			estado.teclas.l = GL_FALSE;
			break;
		case'w':
		case'W':
			estado.teclas.w = GL_FALSE;
			andar = 0;
			break;
		case'a':
		case'A':
			estado.teclas.a = GL_FALSE;
			break;
		case's':
		case'S':
			estado.teclas.s = GL_FALSE;
			andar = 0;
			break;
		case'd':
		case'D':
			estado.teclas.d = GL_FALSE;
			break;
		case 'e':
		case 'E':
			controller->ColocarBomba();
			break;
		case'o':
		case'O':
			controller->ChangeCamera();
			break;
		}
		if (DEBUG)
			printf("Largou a tecla %c\n", key);
	}

	/* Callback para interaccao via teclas especiais  (carregar na tecla) */
	void SpecialKey(int key, int x, int y)
	{
		/* ... accoes sobre outras teclas especiais ...
		GLUT_KEY_F1 ... GLUT_KEY_F12
		GLUT_KEY_UP
		GLUT_KEY_DOWN
		GLUT_KEY_LEFT
		GLUT_KEY_RIGHT
		GLUT_KEY_PAGE_UP
		GLUT_KEY_PAGE_DOWN
		GLUT_KEY_HOME
		GLUT_KEY_END
		GLUT_KEY_INSERT
		*/

		switch (key) {
		case GLUT_KEY_UP:estado.teclas.i = GL_TRUE;
			break;
		case GLUT_KEY_DOWN:estado.teclas.j = GL_TRUE;
			break;
		case GLUT_KEY_LEFT:estado.teclas.k = GL_TRUE;
			break;
		case GLUT_KEY_RIGHT:estado.teclas.l = GL_TRUE;
			break;
		}


		if (DEBUG)
			printf("Carregou na tecla especial %d\n", key);
	}

	/* Callback para interaccao via teclas especiais (largar na tecla) */
	void SpecialKeyUp(int key, int x, int y)
	{

		switch (key) {
		case GLUT_KEY_UP:estado.teclas.i = GL_FALSE;
			break;
		case GLUT_KEY_DOWN:estado.teclas.j = GL_FALSE;
			break;
		case GLUT_KEY_LEFT:estado.teclas.k = GL_FALSE;
			break;
		case GLUT_KEY_RIGHT:estado.teclas.l = GL_FALSE;
			break;
		}

		if (DEBUG)
			printf("Largou a tecla especial %d\n", key);

	}

	void Mouse(int button, int state, int x, int y)
	{
		/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
		state  => GLUT_UP, GLUT_DOWN
		x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
		*/

		if (state == GLUT_UP)
		{
			switch (button) {
			case GLUT_LEFT_BUTTON:

				break;
			case GLUT_MIDDLE_BUTTON:

				break;
			case GLUT_RIGHT_BUTTON:

				break;
			}
		}
		if (DEBUG)
			printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
	}

	void Init() {
		setLight();
		setMaterial();
		Configurations::setScaleCamera(0.2);
		estado.music = new  Sound(Configurations::getGameMusicPath(), 0);
		estado.music->InitAudio(true);
		estado.timerFlag = GL_TRUE;
		estado.doubleBuffer = 1;
		estado.delayMovimento = 50;
		estado.fovCamera = 60;
		estado.raioCamera = 1;
		estado.rotacaoCameraVertical = 30; // sin(30�)=0.5 my ideal angle
		controller = new Game1vsPCController(estado.music);
		/* Registar callbacks do GLUT */
		glEnable(GL_DEPTH_TEST);
		/* callbacks de janelas/desenho */
		glutReshapeFunc(Reshape);
		glutDisplayFunc(Draw);

		/* Callbacks de teclado */
		glutKeyboardFunc(Key);
		glutKeyboardUpFunc(KeyUp);

		/* callbacks rato */
		glutMouseFunc(Mouse);
		glutPassiveMotionFunc(NULL);

		/* callbacks timer/idle */
		glutTimerFunc(estado.delayMovimento, Timer, 0);
	}
}

