#pragma once
#include "operation.h"
class sub : public operation
{
public:
	sub();
	~sub();
	void operate(int* add, int* toThis) override;
	void reverse(int* add, int* toThis) override;
};