#pragma once
#include "Bomb.h"
class BoxBomb : public Bomb
{
public:
	BoxBomb(GLint xPos, GLint zPos);
	~BoxBomb();
	std::vector<GLint*> Explode() override;
};